# (C) 2020 Boud Roukema GPL-3+

# Dummy routines for healpy when healpy is called but not needed
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


# This file provides dummy routines for healpy from the healpix software
# package to bypass the macOS/Darwin/xnu bug currently discussed at:
# 
# https://codeberg.org/boud/elaphrocentre/issues/8
#
# This dummy package should allow python packages such as 'Revolver'
# that depend on healpy to run correctly, provided that the options
# needing healpy are not used.

dummy = "Dummy routine: the real healpix/healpy source code needs to be installed."

def read_map(mask_file, verbose):
    raise RuntimeError(dummy)
    return 1

def get_nside(mask_file):
    raise RuntimeError(dummy)
    return 1

def ang2pix(nside, dec, ra):
    raise RuntimeError(dummy)
    return 1

def pix2ang(nside, dec, ra):
    raise RuntimeError(dummy)
    return 1

def write_map(mask_file, mask):
    raise RuntimeError(dummy)
    return 1

def ud_grade(mask, nside):
    raise RuntimeError(dummy)
    return 1

def get_all_neighbours(nside, theta, phi):
    raise RuntimeError(dummy)
    return 1
