/*
   read_gadget - read gadget format date file

   (C) 2019-2020 Marius Peper GPL-3+
   (C) 2020 Boud Roukema - free unused pointers; TODO comments

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include<stdio.h>
#include<stdlib.h>
#include"read_gadget.h"

int get_particles(char *filename)
{
  struct gadget_header header;
  FILE *fp;
  int Npart;

  fp = fopen(filename, "rb");

  if(NULL == fp){
    Npart = 0;
  }else{
    unsigned int BlockBytes;

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    //printf("sizeof(BlockBytes)=%lu  sizeof(header)=%lu\n",sizeof(BlockBytes),sizeof(header));

    // read in the whole header or just skip it?
    int i;
    for(i=0; i<6; i++) fread(&header.num_particles[i],sizeof(header.num_particles[i]),1,fp);
    Npart = header.num_particles[1];
    //printf("N =%d\n",Npart);
    fseek(fp,sizeof(BlockBytes)+sizeof(header),0);

    unsigned int NewBytes;
    fread(&NewBytes,sizeof(NewBytes),1,fp);
    fclose(fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }
  };

  return Npart;
}


/* TODO: consider generalising this routine to be optionally
   used to return particle identities, rather than having
   read_ids(.) as a separate function; e.g. use void* as the
   return type. Check how this would comply with POSIX and other
   standards.
*/

float *read_gadget(char *filename)
{

  struct gadget_header header;
  FILE *fp;
  float *out = NULL;

  fp = fopen(filename, "rb");

  if(NULL == fp){
    printf("read_gadget Warning: Cannot read file %s\n",
           filename);
  }else{

    unsigned int BlockBytes;

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    //printf("sizeof(BlockBytes)=%lu  sizeof(header)=%lu\n",sizeof(BlockBytes),sizeof(header));

    // read in the whole header or just skip it?
    int i;
    for(i=0; i<6; i++) fread(&header.num_particles[i],sizeof(header.num_particles[i]),1,fp);
    for(i=0; i<6; i++) fread(&header.particle_masses[i],sizeof(header.particle_masses[i]),1,fp);
    fread(&header.scale_factor,sizeof(header.scale_factor),1,fp);
    fread(&header.redshift,sizeof(header.redshift),1,fp);
    fread(&header.flag_sfr,sizeof(header.flag_sfr),1,fp);
    fread(&header.flag_feedback,sizeof(header.flag_feedback),1,fp);
    for(i=0; i<6; i++) fread(&header.num_total_particles[i],sizeof(header.num_total_particles[i]),1,fp);
    fread(&header.flag_cooling,sizeof(header.flag_cooling),1,fp);
    fread(&header.num_files_per_snapshot,sizeof(header.num_files_per_snapshot),1,fp);
    fread(&header.box_size,sizeof(header.box_size),1,fp);
    fread(&header.omega_0,sizeof(header.omega_0),1,fp);
    fread(&header.omega_lambda,sizeof(header.omega_lambda),1,fp);
    fread(&header.h_0,sizeof(header.h_0),1,fp);
    fread(&header.flag_stellarage,sizeof(header.flag_stellarage),1,fp);
    fread(&header.flag_metals,sizeof(header.flag_metals),1,fp);
    for(i=0; i<6; i++) fread(&header.num_total_particles_hw[i],sizeof(header.num_total_particles_hw[i]),1,fp);
    fread(&header.flag_entropy_ics,sizeof(header.flag_entropy_ics),1,fp);
    for(i=0; i<14; i++) header.free[i]=0;

    int Npart;
    Npart = header.num_particles[1];
    fseek(fp,sizeof(BlockBytes)+sizeof(header),0);

    unsigned int NewBytes;
    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    float *Pos;
    Pos=malloc(sizeof(float)*Npart*3);

    for(i=0; i<Npart; i++){
      fread(&Pos[3*i],sizeof(float),1,fp);
      fread(&Pos[3*i+1],sizeof(float),1,fp);
      fread(&Pos[3*i+2],sizeof(float),1,fp);
      //printf("%f  %f  %f\n",Pos[3*i],Pos[3*i+1],Pos[3*i+2]);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    float *Vel;
    Vel=malloc(sizeof(float)*Npart*3);

    for(i=0; i<Npart; i++){
      fread(&Vel[3*i],sizeof(float),1,fp);
      fread(&Vel[3*i+1],sizeof(float),1,fp);
      fread(&Vel[3*i+2],sizeof(float),1,fp);
      //printf("%f  %f  %f\n",Pos[3*i],Pos[3*i+1],Pos[3*i+2]);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    long int *id;
    id=malloc(sizeof(long int)*Npart);

    for(i=0; i<Npart; i++){
      fread(&id[i],sizeof(long int),1,fp);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    float *mass;
    mass=malloc(sizeof(float)*Npart);

    for(i=0; i<Npart; i++){
      fread(&mass[i],sizeof(float),1,fp);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    float *pot;
    pot=malloc(sizeof(float)*Npart);

    for(i=0; i<Npart; i++){
      fread(&pot[i],sizeof(float),1,fp);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    // hard coded because I want to have the potential in Revolver. More flexible would be nicer
    out=malloc(sizeof(float)*Npart*8);

    for(i=0; i<Npart; i++){
      out[4*i]=Pos[3*i]*header.scale_factor;
      out[4*i+1]=Pos[3*i+1]*header.scale_factor;
      out[4*i+2]=Pos[3*i+2]*header.scale_factor;
      out[4*i+3]=pot[i];
      //printf("Pos_x[%d]=%f   pot[%d]=%f\n",i,out[i],i,out[4*i+3]);
    }

    /* Do not free 'out', because it is the pointer that is returned to the
       calling routine. */
    free(pot);
    free(mass);
    free(id);
    free(Vel);
    free(Pos);
  }; /* if(NULL != fp) */

  return out;
}


long int *read_ids(char *filename)
{

  struct gadget_header header;
  FILE *fp;
  long int *id = NULL;

  fp = fopen(filename, "rb");

  if(NULL != fp){

    unsigned int BlockBytes;

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    //printf("sizeof(BlockBytes)=%lu  sizeof(header)=%lu\n",sizeof(BlockBytes),sizeof(header));

    // read in the whole header or just skip it?
    int i;
    for(i=0; i<6; i++) fread(&header.num_particles[i],sizeof(header.num_particles[i]),1,fp);
    for(i=0; i<6; i++) fread(&header.particle_masses[i],sizeof(header.particle_masses[i]),1,fp);
    fread(&header.scale_factor,sizeof(header.scale_factor),1,fp);
    fread(&header.redshift,sizeof(header.redshift),1,fp);
    fread(&header.flag_sfr,sizeof(header.flag_sfr),1,fp);
    fread(&header.flag_feedback,sizeof(header.flag_feedback),1,fp);
    for(i=0; i<6; i++) fread(&header.num_total_particles[i],sizeof(header.num_total_particles[i]),1,fp);
    fread(&header.flag_cooling,sizeof(header.flag_cooling),1,fp);
    fread(&header.num_files_per_snapshot,sizeof(header.num_files_per_snapshot),1,fp);
    fread(&header.box_size,sizeof(header.box_size),1,fp);
    fread(&header.omega_0,sizeof(header.omega_0),1,fp);
    fread(&header.omega_lambda,sizeof(header.omega_lambda),1,fp);
    fread(&header.h_0,sizeof(header.h_0),1,fp);
    fread(&header.flag_stellarage,sizeof(header.flag_stellarage),1,fp);
    fread(&header.flag_metals,sizeof(header.flag_metals),1,fp);
    for(i=0; i<6; i++) fread(&header.num_total_particles_hw[i],sizeof(header.num_total_particles_hw[i]),1,fp);
    fread(&header.flag_entropy_ics,sizeof(header.flag_entropy_ics),1,fp);
    for(i=0; i<14; i++) header.free[i]=0;

    int Npart;
    Npart = header.num_particles[1];
    fseek(fp,sizeof(BlockBytes)+sizeof(header),0);

    unsigned int NewBytes;
    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    float *Pos;
    Pos=malloc(sizeof(float)*Npart*3);

    for(i=0; i<Npart; i++){
      fread(&Pos[3*i],sizeof(float),1,fp);
      fread(&Pos[3*i+1],sizeof(float),1,fp);
      fread(&Pos[3*i+2],sizeof(float),1,fp);
      //printf("%f  %f  %f\n",Pos[3*i],Pos[3*i+1],Pos[3*i+2]);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    float *Vel;
    Vel=malloc(sizeof(float)*Npart*3);

    for(i=0; i<Npart; i++){
      fread(&Vel[3*i],sizeof(float),1,fp);
      fread(&Vel[3*i+1],sizeof(float),1,fp);
      fread(&Vel[3*i+2],sizeof(float),1,fp);
      //printf("%f  %f  %f\n",Pos[3*i],Pos[3*i+1],Pos[3*i+2]);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    id=malloc(sizeof(long int)*Npart);

    for(i=0; i<Npart; i++){
      fread(&id[i],sizeof(long int),1,fp);
    }

    /* Reading further in the file provides a
       sanity check that the file is readable
       in the expected format. */

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    float *mass;
    mass=malloc(sizeof(float)*Npart);

    for(i=0; i<Npart; i++){
    fread(&mass[i],sizeof(float),1,fp);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    float *pot;
    pot=malloc(sizeof(float)*Npart);

    for(i=0; i<Npart; i++){
      fread(&pot[i],sizeof(float),1,fp);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    free(pot);
    free(mass);
    /* Do not free 'id', because it is the pointer that is returned to the
       calling routine. */
    free(Vel);
    free(Pos);
  };

  return id;
}


int main()
{
  char *filename="output";
  int Npart;
  float *Pos;
  int i;

  Npart=get_particles(filename);
  Pos=malloc(sizeof(float)*Npart*4);
  Pos=read_gadget(filename);
  for(i=0; i<Npart; i++){
    printf("%f  %f  %f  %f\n",Pos[4*i],Pos[4*i+1],Pos[4*i+2],Pos[4*i+3]);
  }
  return 0;
}
