#!/usr/bin/env python

# plot_voidgals_properties - galaxies' relationship with their void
# (C) 2020 Boud Roukema, Marius Peper GPL-3+
#
# Fit and plot dependence of galaxies' infall history properties
# in relation to void properties such as how much of the galaxy's
# host halo consists of void particles and the elaphrocentric distance
# of the host halo.
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.markers as markers

import numpy as np
from numpy.random import SeedSequence, default_rng

from theil_sen_mod import theil_sen, theil_sen_robust_stderr
from theil_sen_2param_mod import theil_sen_2param, theil_sen_2param_robust_stderr

debug = False
N_ThSen_threshold = 200
NSigCrit = 3

def voidgals_props_read_data(voidgals_filename):
    """
    maximum_allowed_fit_amplitude: float
         Data will be omitted where the amplitude is greater than this value
    """

    # Read void galaxy properties
    frac_in_void_in = np.loadtxt(voidgals_filename, usecols=0, dtype=np.float)
    rel_dist_in = np.loadtxt(voidgals_filename, usecols=1, dtype=np.float)
    infall_amp_in = np.loadtxt(voidgals_filename, usecols=2, dtype=np.float)
    infall_tau_in = np.loadtxt(voidgals_filename, usecols=3, dtype=np.float)
    galaxy_size_in = np.loadtxt(voidgals_filename, usecols=4, dtype=np.float)
    galaxy_spin_in = np.loadtxt(voidgals_filename, usecols=5, dtype=np.float)
    galaxy_Rvir_in = np.loadtxt(voidgals_filename, usecols=6, dtype=np.float)

    # The validity of infall fits has to be checked when analysing or
    # plotting the data further; galaxy sizes are valid even when infall
    # fits fail.
    frac_in_void = frac_in_void_in
    rel_dist = rel_dist_in
    infall_amp = infall_amp_in
    infall_tau = infall_tau_in
    galaxy_size = galaxy_size_in
    galaxy_spin = galaxy_spin_in
    galaxy_Rvir = galaxy_Rvir_in


    # Number of galaxies
    if(0==frac_in_void.size):
        N_gals = 0
        print("void_gals_properties: void_gals_read_data: No valid data found in file ",
              voidgals_filename)
        return (N_gals, frac_in_void, rel_dist, infall_amp, infall_tau,
                galaxy_size, galaxy_spin, galaxy_Rvir)

    N_gals = (frac_in_void.shape)[0]
    print('N_gals=', N_gals)

    return (N_gals, frac_in_void, rel_dist, infall_amp, infall_tau,
            galaxy_size, galaxy_spin, galaxy_Rvir)

def find_stable_N_full_check(xdata, ydata, N_gals, N_in):
    """
    This is a brute-force routine to empirically check how fast a stable
    Theil-Sen slope estimate can be obtained using a small sub-sample
    of the full set of pairs of data points.

    INPUTS:
    xdata - numpy array of floats
    ydata - numpy array of floats
    N_gals - number of objects in the two arrays
    N_in - input guess at sufficient highly value of N_max_sample

    RETURNS:
    N_new - sufficiently high value of N_max_sample to give to theil_sen()
    slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero -
          last set of parameters tested, including the uncertainties
    """

    sig_want = 0.1 # required stability to within sig_want * sigma

    N_old = N_in

    N_new = np.int(N_old * 1.1)
    slope_base, zero_base = theil_sen(xdata, ydata, N_max_sample = N_old)
    slope_test, zero_test = theil_sen(xdata, ydata, N_max_sample = N_new)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(xdata, ydata, N_max_sample = N_old)

    N_stop_trying = 10*(np.int(N_gals*np.sqrt(N_gals))+1)

    while( ((np.abs((slope_base-slope_test)))
            > sig_want*sig_pslope) or
           ((np.abs((zero_base-zero_test)))
            > sig_want*sig_pzero) ):
        if(N_new >= N_stop_trying):
            return N_new, slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero
        N_old = N_new
        N_new = np.int(N_old * 1.1)
        slope_base, zero_base = theil_sen(xdata, ydata, N_max_sample = N_old)
        slope_test, zero_test = theil_sen(xdata, ydata, N_max_sample = N_new)

    print("find_stable_N_full_check: (np.abs((slope_base-slope_test))),"+
             "sig_want*sig_pslope),"+
             "(np.abs((zero_base-zero_test))),"+
             "sig_want*sig_pzero) ) = ",
          (np.abs((slope_base-slope_test))),
          sig_want*sig_pslope,
          (np.abs((zero_base-zero_test))),
          sig_want*sig_pzero )

    return N_new, slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero


def theil_sen_try_fast(xdata, ydata, N_max_sample = -99):
    """This routine tries to do Theil-Sen robust linear fitting without using
    the full set of pairs of data points, by first trying a random
    subset. If this is insufficiently accurate, then the full set is
    is used.

    When called by theil_sen_try_fast, theil_sen() is given the N_max_sample parameter
    explicitly. A value of N_max_sample different from -1 tells theil_sen
    to only calculate N_max_sample pairs, instead of N_max_sample^2 pairs.
    (In principle, only N_max_sample*(N_max_sample-1)/2 pairs need to be checked;
    doing this in python while optimising for speed are open questions.)

    Theil_sen_try_fast can call theil_sen up to four times. Let us write N
    for the number of xdata values. Before the while loop, we check
    theil_sen using N_max_sample = N and then using N^1.5, and we assume
    that using N is accurate enough to estimate the errors with
    theil_sen_robust_stderr.

    If the N and N^1.5 estimates disagree too much, then we continue to the
    while loop. In the first iteration, we try out N^1.8. If that gives
    sufficient agreement with the N^1.5 estimate, then we accept that N^1.8
    result. If not, we continue to the second iteration, in which we
    set N_max_sample = -1, which means we effectively use N^2 pairs.
    We accept this as the full calculation.


    INPUTS:
    xdata - numpy array of floats
    ydata - numpy array of floats
    N_max_sample - ignored; kept for consistency with theil_sen parameter list

    RETURNS:
    slope_new, zero_new

    """

    sig_want = 0.1 # required stability to within sig_want * sigma

    N_old = xdata.size
    N_old_check = ydata.size
    if(N_old != N_old_check):
        raise RuntimeError("theil_sen_try_fast: N_old for xdata != N_old_check for ydata",N_old, N_old_check)

    ## Two tries [0], [1] will be made with N_old^1.5 and N_old^1.8.  If
    ## they are insufficiently accurate, then the full set of pairs will be
    ## used (try [2]).

    N_new = [ np.int(N_old*np.sqrt(N_old))+1,
              np.int(np.float(N_old)**1.8),
              -1]

    i_try = 0
    slope_old, zero_old = theil_sen(xdata, ydata, N_max_sample = N_old)
    slope_new, zero_new = theil_sen(xdata, ydata, N_max_sample = N_new[i_try])
    sig_pslope, sig_pzero = theil_sen_robust_stderr(xdata, ydata, N_max_sample = N_old)

    while( ( ((np.abs((slope_old-slope_new)))
              > sig_want*sig_pslope) or
             ((np.abs((zero_old-zero_new)))
              > sig_want*sig_pzero) ) and
           (i_try+1 <= 2) ):

        # Copy the previous 'new' values to the old values
        N_old = N_new[i_try] # for the old value of i_try
        slope_old = slope_new
        zero_old = zero_new
        i_try += 1
        slope_new, zero_new = theil_sen(xdata, ydata, N_max_sample = N_new[i_try])

    print("theil_sen_try_fast: (np.abs((slope_old-slope_new))),"+
             "sig_want*sig_pslope),"+
             "(np.abs((zero_old-zero_new))),"+
             "sig_want*sig_pzero) ) = ",
          (np.abs((slope_old-slope_new))),
          sig_want*sig_pslope,
          (np.abs((zero_old-zero_new))),
          sig_want*sig_pzero )

    return slope_new, zero_new



def decide_show_fit(pslope, sig_pslope, NSigCrit):
    """
    This routine checks if the slope of a theil-sen fit is significant
    enough to be shown in the created pdf.

    INPUTS:
    pslope - float
    sig_pslope - float
    NSigCrit - float

    RETURNS:
    show_fit - string
    """
    NSig = np.abs(pslope/sig_pslope)

    if (NSig>=NSigCrit):
        show_fit = True
    else:
        show_fit = False

    return show_fit


def voidgals_props_plot(N_gals, frac_in_void, rel_dist, infall_amp_in, infall_tau_in,
                        galaxy_size, galaxy_spin, galaxy_Rvir,
                        maximum_allowed_fit_amplitude = 100.0,
                        min_allowed_abs_fit_amplitude = 1e-30):

    seeds = SeedSequence(7703)
    seed = seeds.spawn(2)
    rng = default_rng(seed[0])

    calibrate_TS_convergence = False


    if os.environ.get('METADATA_CREATOR'):
        metadata_creator = os.environ.get('METADATA_CREATOR')
    else:
        metadata_creator = 'elaphrocentre'
    metadata = {"Creator": metadata_creator}

    # Find valid indices where the infall fits were successful
    valid_i = (np.array(np.where(
        (infall_amp_in <= maximum_allowed_fit_amplitude)
        &
        (np.abs(infall_amp_in) > min_allowed_abs_fit_amplitude) )))[0]

    show_fit = True # by default, show all fits to avoid selection bias

    if(valid_i.size < 2):
        raise RuntimeError("plot_voidgals_properties: voidgals_props_plot: Error: Too few valid infall fits.")

    # Select valid infall amplitudes and decay rates
    infall_amp = infall_amp_in[valid_i]
    infall_tau = infall_tau_in[valid_i]

    # Select void fractions and fractional distances for infall statistics
    frac_in_void_valid = frac_in_void[valid_i]
    rel_dist_valid = rel_dist[valid_i]

    # infall_amp dependence on frac_in_void

    # Ask theil_sen to only test a subsample of pairs if there
    # are too many galaxies.
    if(N_gals > N_ThSen_threshold):
        #N_ThSen_sample = N_ThSen_threshold
        N_ThSen_sample = np.int(N_gals*np.sqrt(N_gals))+1 # avoid N^2
        N_ThSen_sample_2param = 3*N_ThSen_threshold
    else:
        N_ThSen_sample = -1
        N_ThSen_sample_2param = 3*N_gals

    if(calibrate_TS_convergence):
        # Estimate how many galaxies are needed to get a stable result.
        # Use 3 randomly chosen datasets to estimate the maximal needed N
        # to get stable results.

        N_ThSen_sample_amp, slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero = (
            find_stable_N_full_check(frac_in_void_valid, infall_amp, N_gals, N_ThSen_sample) )
        print("N_ThSen_sample_amp=",N_ThSen_sample_amp)
        print(" ...  slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero = ",
              slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero)

        N_ThSen_sample_size, slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero = (
            find_stable_N_full_check(frac_in_void, galaxy_size, N_gals, N_ThSen_sample) )
        print("N_ThSen_sample_size=",N_ThSen_sample_size)
        print(" ...  slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero = ",
              slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero)

        N_ThSen_sample_Rvir, slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero = (
            find_stable_N_full_check(rel_dist, galaxy_Rvir, N_gals, N_ThSen_sample) )
        print("N_ThSen_sample_Rvir=",N_ThSen_sample_Rvir)
        print(" ...  slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero = ",
              slope_base, slope_test, sig_pslope, zero_base, zero_test, sig_pzero)

        N_ThSen_sample = max(N_ThSen_sample_amp,N_ThSen_sample_size,N_ThSen_sample_Rvir)



    N_ThSen_sample_2param = 3*N_ThSen_threshold # this is not really useful

    xlims_frac = (1.05, 0.5)
    xlims_reldist = (0, 2.0)

    ylims_amp = (0.0,10.0)
    ylims_tau = (-100.0, 100.0)
    ylims_size = (0.0, 30.0)
    ylims_spin = (0.0, 0.3)
    ylims_Rvir = (0.0, 400.0)

    ylims_frac = (0.5, 1.05) # same as xlims but swapped

    voidgals_props_file = open("voidgals_props_TS.dat","w+")


    ## Check relation between fraction of particles in void and
    ## relative distance in units of the void effective radius.

    pslope, pzero = theil_sen(rel_dist, frac_in_void, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(rel_dist, frac_in_void, N_max_sample = N_ThSen_sample)
    print("frac_in_void vs rel_dist: pslope, pzero=",pslope, pzero)
    print("frac_in_void vs rel_dist: sig_slope, sig_pzero=",sig_pslope, sig_pzero)

    voidgals_props_file.write("frac_rreff_slope = %g\n" % pslope)
    voidgals_props_file.write("frac_rreff_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("frac_rreff_zero = %g\n" % pzero)
    voidgals_props_file.write("frac_rreff_sig_zero = %g\n" % sig_pzero)

    show_fit = False # arbitrary request by referee

    xlims = xlims_reldist
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    for plot_variation in ['_nofit','']:
        # Plot
        plt.clf()
        plt.figure()
        plt.rcParams.update({'font.size': 18,
                             'font.family': 'serif'})

        plt.plot(rel_dist, frac_in_void,
                 'o', linewidth=1, markersize=1)
        #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
        if (show_fit):
            plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

        show_fit = True # reset

        plt.xlim(xlims)
        plt.ylim(ylims_frac)
        plt.xlabel(r'elaphrocentric position $r/R_{\mathrm{eff}}$')
        plt.ylabel(r'fraction in void $f_{\,\cal{H}\cap \cal{V}}$')

        plt.tight_layout()
        plt.savefig('frac_rreff'+plot_variation+'.eps', metadata=metadata)
        plt.close()


    print("========= infall_amp = ",infall_amp)

    pslope, pzero = theil_sen(frac_in_void_valid, infall_amp, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(frac_in_void_valid, infall_amp, N_max_sample = N_ThSen_sample)
    print("infall_amp vs frac_in_void: pslope, pzero=",pslope, pzero)
    print("infall_amp vs frac_in_void: sig_slope, sig_pzero=",sig_pslope, sig_pzero)

    voidgals_props_file.write("infall_amp_frac_slope = %g\n" % pslope)
    voidgals_props_file.write("infall_amp_frac_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("infall_amp_frac_zero = %g\n" % pzero)
    voidgals_props_file.write("infall_amp_frac_sig_zero = %g\n" % sig_pzero)

    show_fit = False # arbitrary request by referee

    xlims = xlims_frac
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    for plot_variation in ['_nofit','']:
        plt.clf()
        plt.figure()
        plt.rcParams.update({'font.size': 18,
                             'font.family': 'serif'})

        plt.plot(frac_in_void_valid, infall_amp,
                 'o', linewidth=1, markersize=1)
        #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
        if (show_fit):
            plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

        show_fit = True # restore default

        plt.xlim(xlims)
        plt.ylim(ylims_amp)
        plt.xlabel(r'fraction in void $f_{\,\cal{H}\cap \cal{V}}$')
        plt.ylabel(r'infall amplitude $A (M_{\odot}/\mathrm{yr})$ ')

        plt.tight_layout()
        plt.savefig('infall_frac_amp'+plot_variation+'.eps', metadata=metadata)
        plt.close()


    # infall_tau dependence on frac_in_void

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen_try_fast(frac_in_void_valid, infall_tau, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(frac_in_void_valid, infall_tau,
                                                    N_max_sample = N_ThSen_sample)
    print("infall_tau vs frac_in_void: pslope, pzero=",pslope, pzero)
    print("infall_tau vs frac_in_void: sig_slope, sig_pzero=",sig_pslope, sig_pzero)

    voidgals_props_file.write("infall_tau_frac_slope = %g\n" % pslope)
    voidgals_props_file.write("infall_tau_frac_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("infall_tau_frac_zero = %g\n" % pzero)
    voidgals_props_file.write("infall_tau_frac_sig_zero = %g\n" % sig_pzero)

    xlims = xlims_frac # unchanged
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    plt.clf()
    plt.figure()
    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})

    plt.plot(frac_in_void_valid, infall_tau,
             'o', linewidth=1, markersize=1)
    #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
    if (show_fit):
        plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

    plt.xlim(xlims)
    plt.ylim(ylims_tau)
    plt.xlabel(r'fraction in void $f_{\,\cal{H}\cap \cal{V}}$')
    plt.ylabel(r'infall decay rate $\tau$ (Gyr)')

    plt.tight_layout()
    plt.savefig('infall_frac_tau.eps', metadata=metadata)
    plt.close()


    # galaxy_size dependence on frac_in_void

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen_try_fast(frac_in_void, galaxy_size, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(frac_in_void, galaxy_size,
                                                    N_max_sample = N_ThSen_sample)

    voidgals_props_file.write("galsize_frac_slope = %g\n" % pslope)
    voidgals_props_file.write("galsize_frac_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("galsize_frac_zero = %g\n" % pzero)
    voidgals_props_file.write("galsize_frac_sig_zero = %g\n" % sig_pzero)

    show_fit = False # arbitrary request by referee

    xlims = xlims_frac # unchanged
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    for plot_variation in ['_nofit','']:
        plt.clf()
        plt.figure()
        plt.rcParams.update({'font.size': 18,
                             'font.family': 'serif'})

        plt.plot(frac_in_void, galaxy_size,
                 'o', linewidth=1, markersize=1)
        #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
        if (show_fit):
            plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

        show_fit = True # restore default

        plt.xlim(xlims)
        plt.ylim(ylims_size)
        plt.xlabel(r'fraction in void $f_{\,\cal{H}\cap \cal{V}}$')
        plt.ylabel(r'disk scale $r_{\mathrm{disk}}$ (kpc/$h$)')

        plt.tight_layout()
        plt.savefig('galaxy_size_frac'+plot_variation+'.eps', metadata=metadata)
        plt.close()


    # galaxy_spin dependence on frac_in_void

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen_try_fast(frac_in_void, galaxy_spin, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(frac_in_void, galaxy_spin,
                                                    N_max_sample = N_ThSen_sample)

    voidgals_props_file.write("galspin_frac_slope = %g\n" % pslope)
    voidgals_props_file.write("galspin_frac_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("galspin_frac_zero = %g\n" % pzero)
    voidgals_props_file.write("galspin_frac_sig_zero = %g\n" % sig_pzero)

    xlims = xlims_frac # unchanged
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    plt.clf()
    plt.figure()
    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})

    plt.plot(frac_in_void, galaxy_spin,
             'o', linewidth=1, markersize=1)
    #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
    if (show_fit):
        plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

    plt.xlim(xlims)
    plt.ylim(ylims_spin)
    plt.xlabel(r'fraction in void $f_{\,\cal{H}\cap \cal{V}}$')
    plt.ylabel(r'spin parameter $\lambda$')

    plt.tight_layout()
    plt.savefig('galaxy_spin_frac.eps', metadata=metadata)
    plt.close()


    # galaxy_Rvir dependence on frac_in_void

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen_try_fast(frac_in_void, galaxy_Rvir, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(frac_in_void, galaxy_Rvir,
                                                    N_max_sample = N_ThSen_sample)

    voidgals_props_file.write("galRvir_frac_slope = %g\n" % pslope)
    voidgals_props_file.write("galRvir_frac_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("galRvir_frac_zero = %g\n" % pzero)
    voidgals_props_file.write("galRvir_frac_sig_zero = %g\n" % sig_pzero)

    xlims = xlims_frac # unchanged
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    plt.clf()
    plt.figure()
    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})

    plt.plot(frac_in_void, galaxy_Rvir,
             'o', linewidth=1, markersize=1)
    #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
    if (show_fit):
        plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

    plt.xlim(xlims)
    plt.ylim(ylims_Rvir)
    plt.xlabel(r'fraction in void $f_{\,\cal{H}\cap \cal{V}}$')
    plt.ylabel(r'$R_{\mathrm{vir}}$ (kpc/$h$)')

    plt.tight_layout()
    plt.savefig('galaxy_Rvir_frac.eps', metadata=metadata)
    plt.close()



    ### Dependences on rel_dist

    # infall_amp dependence on rel_dist

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen(rel_dist_valid, infall_amp, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(rel_dist_valid, infall_amp,
                                                    N_max_sample = N_ThSen_sample)
    print("infall_amp vs rel_dist: pslope, pzero=",pslope, pzero)
    print("infall_amp vs rel_dist: sig_pslope, sig_pzero=",sig_pslope, sig_pzero)

    voidgals_props_file.write("infall_amp_reldist_slope = %g\n" % pslope)
    voidgals_props_file.write("infall_amp_reldist_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("infall_amp_reldist_zero = %g\n" % pzero)
    voidgals_props_file.write("infall_amp_reldist_sig_zero = %g\n" % sig_pzero)

    show_fit = False # arbitrary request by referee

    xlims = xlims_reldist
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    for plot_variation in ['_nofit','']:
        plt.clf()
        plt.figure()
        plt.rcParams.update({'font.size': 18,
                             'font.family': 'serif'})

        plt.plot(rel_dist_valid, infall_amp,
                 'o', linewidth=1, markersize=1)
        #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
        if (show_fit):
            plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

        show_fit = True # restore default

        plt.xlim(xlims)
        plt.ylim(ylims_amp)
        plt.xlabel(r'elaphrocentric position $r/R_{\mathrm{eff}}$')
        plt.ylabel(r'infall amplitude $A (M_{\odot}/\mathrm{yr})$ ')

        plt.tight_layout()
        plt.savefig('infall_rreff_amp'+plot_variation+'.eps', metadata=metadata)
        plt.close()


    # infall_tau dependence on rel_dist

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen_try_fast(rel_dist_valid, infall_tau, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(rel_dist_valid, infall_tau,
                                                    N_max_sample = N_ThSen_sample)
    print("infall_tau vs rel_dist: pslope, pzero=",pslope, pzero)
    print("infall_tau vs rel_dist: sig_pslope, sig_pzero=",sig_pslope, sig_pzero)

    voidgals_props_file.write("infall_tau_reldist_slope = %g\n" % pslope)
    voidgals_props_file.write("infall_tau_reldist_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("infall_tau_reldist_zero = %g\n" % pzero)
    voidgals_props_file.write("infall_tau_reldist_sig_zero = %g\n" % sig_pzero)


    xlims = xlims_reldist # unchanged
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    plt.clf()
    plt.figure()
    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})

    plt.plot(rel_dist_valid, infall_tau,
             'o', linewidth=1, markersize=1)
    #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
    if (show_fit):
        plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

    plt.xlim(xlims)
    plt.ylim(ylims_tau)
    plt.xlabel(r'elaphrocentric position $r/R_{\mathrm{eff}}$')
    plt.ylabel(r'infall decay rate $\tau$ (Gyr)')

    plt.tight_layout()
    plt.savefig('infall_rreff_tau.eps', metadata=metadata)
    plt.close()


    # galaxy_size dependence on rel_dist

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen_try_fast(rel_dist, galaxy_size, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(rel_dist, galaxy_size,
                                                    N_max_sample = N_ThSen_sample)

    voidgals_props_file.write("galsize_reldist_slope = %g\n" % pslope)
    voidgals_props_file.write("galsize_reldist_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("galsize_reldist_zero = %g\n" % pzero)
    voidgals_props_file.write("galsize_reldist_sig_zero = %g\n" % sig_pzero)


    xlims = xlims_reldist # unchanged
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    plt.clf()
    plt.figure()
    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})

    plt.plot(rel_dist, galaxy_size,
             'o', linewidth=1, markersize=1)
    #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
    if (show_fit):
        plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

    plt.xlim(xlims)
    plt.ylim(ylims_size)
    plt.xlabel(r'elaphrocentric position $r/R_{\mathrm{eff}}$')
    plt.ylabel(r'disk scale $r_{\mathrm{disk}}$ (kpc/$h$)')

    plt.tight_layout()
    plt.savefig('galaxy_size_rreff.eps', metadata=metadata)
    plt.close()


    # galaxy_spin dependence on rel_dist

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen_try_fast(rel_dist, galaxy_spin, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(rel_dist, galaxy_spin,
                                                    N_max_sample = N_ThSen_sample)

    voidgals_props_file.write("galspin_reldist_slope = %g\n" % pslope)
    voidgals_props_file.write("galspin_reldist_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("galspin_reldist_zero = %g\n" % pzero)
    voidgals_props_file.write("galspin_reldist_sig_zero = %g\n" % sig_pzero)


    xlims = xlims_reldist # unchanged
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    plt.clf()
    plt.figure()
    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})

    plt.plot(rel_dist, galaxy_spin,
             'o', linewidth=1, markersize=1)
    #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
    if (show_fit):
        plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

    plt.xlim(xlims)
    plt.ylim(ylims_spin)
    plt.xlabel(r'elaphrocentric position $r/R_{\mathrm{eff}}$')
    plt.ylabel(r'spin parameter $\lambda$')

    plt.tight_layout()
    plt.savefig('galaxy_spin_rreff.eps', metadata=metadata)
    plt.close()


    # galaxy_Rvir dependence on rel_dist

    # Fit the relation for a random subsample
    pslope, pzero = theil_sen_try_fast(rel_dist, galaxy_Rvir, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(rel_dist, galaxy_Rvir,
                                                    N_max_sample = N_ThSen_sample)

    voidgals_props_file.write("galRvir_reldist_slope = %g\n" % pslope)
    voidgals_props_file.write("galRvir_reldist_sig_slope = %g\n" % sig_pslope)
    voidgals_props_file.write("galRvir_reldist_zero = %g\n" % pzero)
    voidgals_props_file.write("galRvir_reldist_sig_zero = %g\n" % sig_pzero)


    xlims = xlims_reldist # unchanged
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    plt.clf()
    plt.figure()
    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})

    plt.plot(rel_dist, galaxy_Rvir,
             'o', linewidth=1, markersize=1)
    #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
    if (show_fit):
        plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

    plt.xlim(xlims)
    plt.ylim(ylims_Rvir)
    plt.xlabel(r'elaphrocentric position $r/R_{\mathrm{eff}}$')
    plt.ylabel(r'$R_{\mathrm{vir}}$ (kpc/$h$)')

    plt.tight_layout()
    plt.savefig('galaxy_Rvir_rreff.eps', metadata=metadata)
    plt.close()

    ## Try to remove Rvir dependence from frac or rel_dist dependence

    # Fit the relation for a random subsample
    pslope0, pslope1, pzero = (
        theil_sen_2param(rel_dist, galaxy_Rvir, galaxy_spin,
                         N_triples_max = N_ThSen_sample_2param) )
    sig_pslope0, sig_pslope1, sig_pzero = (
        theil_sen_2param_robust_stderr(rel_dist, galaxy_Rvir, galaxy_spin,
                                       N_triples_max = N_ThSen_sample_2param) )

    voidgals_props_file.write("galspin_twopar_reldist_notRvir_slope = %g\n" % pslope0)
    voidgals_props_file.write("galspin_twopar_Rvir_notreldist_slope = %g\n" % pslope1)
    voidgals_props_file.write("galspin_twopar_reldist_notRvir_sig_slope = %g\n" % sig_pslope0)
    voidgals_props_file.write("galspin_twopar_Rvir_notreldist_sig_slope = %g\n" % sig_pslope1)
    voidgals_props_file.write("galspin_twopar_reldist_zero = %g\n" % pzero)
    voidgals_props_file.write("galspin_twopar_reldist_sig_zero = %g\n" % sig_pzero)

    # Fit the relation for a random subsample
    pslope0, pslope1, pzero = (
        theil_sen_2param(frac_in_void, galaxy_Rvir, galaxy_spin,
                         N_triples_max = N_ThSen_sample_2param) )
    sig_pslope0, sig_pslope1, sig_pzero = (
        theil_sen_2param_robust_stderr(frac_in_void, galaxy_Rvir, galaxy_spin,
                                       N_triples_max = N_ThSen_sample_2param) )

    voidgals_props_file.write("galspin_twopar_frac_notRvir_slope = %g\n" % pslope0)
    voidgals_props_file.write("galspin_twopar_Rvir_notfrac_slope = %g\n" % pslope1)
    voidgals_props_file.write("galspin_twopar_frac_notRvir_sig_slope = %g\n" % sig_pslope0)
    voidgals_props_file.write("galspin_twopar_Rvir_notfrac_sig_slope = %g\n" % sig_pslope1)
    voidgals_props_file.write("galspin_twopar_frac_zero = %g\n" % pzero)
    voidgals_props_file.write("galspin_twopar_frac_sig_zero = %g\n" % sig_pzero)



    voidgals_props_file.close()


def voidgals_acc_plot(N_voids, eff_rad_array, acc_rad, acc_tan):

    seeds = SeedSequence(8311)
    seed = seeds.spawn(2)
    rng = default_rng(seed[0])

    # Ask theil_sen to only test a subsample of pairs if there
    # are too many galaxies.
    if(N_voids > N_ThSen_threshold):
        N_ThSen_sample = np.int(N_voids*np.sqrt(N_voids))+1 # avoid N^2
    else:
        N_ThSen_sample = -1

    xlims = (0.0, 8.0)
    ylims_rad = (-5.0,5.0)
    ylims_tan = (-0.5,5.0)

    if os.environ.get('METADATA_CREATOR'):
        metadata_creator = os.environ.get('METADATA_CREATOR')
    else:
        metadata_creator = 'elaphrocentre'
    metadata = {"Creator": metadata_creator}

    show_fit = True # by default, show all fits to avoid selection bias

    voidgals_acc_file = open("voidgals_acc_TS.dat","w+")

    pslope, pzero = theil_sen_try_fast(eff_rad_array, acc_rad, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(eff_rad_array, acc_rad, N_max_sample = N_ThSen_sample)
    print("radial acceleration at a certain distance vs effective radius of the void: pslope, pzero=",pslope, pzero)
    print("radial acceleration at a certain distance vs effective radius of the void: sig_slope, sig_pzero=",sig_pslope, sig_pzero)

    voidgals_acc_file.write("acc_rad_slope = %g\n" % pslope)
    voidgals_acc_file.write("acc_rad_sig_slope = %g\n" % sig_pslope)
    voidgals_acc_file.write("acc_rad_zero = %g\n" % pzero)
    voidgals_acc_file.write("acc_rad_sig_zero = %g\n" % sig_pzero)

    #xlims = xlims_frac
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    plt.clf()
    plt.figure()
    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})

    plt.plot(eff_rad_array, acc_rad,
             'o', linewidth=1, markersize=1)
    #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
    if (show_fit):
        plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

    plt.xlim(xlims)
    plt.ylim(ylims_rad)
    plt.xlabel(r'effective void radius $R_{\mathrm{eff}}$ (Mpc/$h$)')
    plt.ylabel(r'rad. accel. $\dot{v}_{\parallel}$ (km/s/Gyr)')

    plt.tight_layout()
    plt.savefig('accel_rad.eps', metadata=metadata)
    plt.close()



    pslope, pzero = theil_sen_try_fast(eff_rad_array, acc_tan, N_max_sample = N_ThSen_sample)
    sig_pslope, sig_pzero = theil_sen_robust_stderr(eff_rad_array, acc_tan, N_max_sample = N_ThSen_sample)
    print("radial acceleration at a certain distance vs effective radius of the void: pslope, pzero=",pslope, pzero)
    print("radial acceleration at a certain distance vs effective radius of the void: sig_slope, sig_pzero=",sig_pslope, sig_pzero)

    voidgals_acc_file.write("acc_tan_slope = %g\n" % pslope)
    voidgals_acc_file.write("acc_tan_sig_slope = %g\n" % sig_pslope)
    voidgals_acc_file.write("acc_tan_zero = %g\n" % pzero)
    voidgals_acc_file.write("acc_tan_sig_zero = %g\n" % sig_pzero)

    show_fit = False # arbitrary request by referee

    #xlims = xlims_frac
    x_theilsen = np.array(xlims)
    y_theilsen = pzero + pslope*x_theilsen

    # Plot
    for plot_variation in ['_nofit','']:
        plt.clf()
        plt.figure()
        plt.rcParams.update({'font.size': 18,
                             'font.family': 'serif'})

        plt.plot(eff_rad_array, acc_tan,
                 'o', linewidth=1, markersize=1)
        #show_fit = decide_show_fit(pslope, sig_pslope, NSigCrit)
        if (show_fit):
            plt.plot(x_theilsen, y_theilsen, '-', linewidth=2)

        show_fit = True # restore default

        plt.xlim(xlims)
        plt.ylim(ylims_tan)
        plt.xlabel(r'effective void radius $R_{\mathrm{eff}}$ (Mpc/$h$)')
        plt.ylabel(r'tang. accel. $\dot{v}_{\perp}$ (km/s/Gyr)')

        plt.tight_layout()
        plt.savefig('accel_tan'+plot_variation+'.eps', metadata=metadata)
        plt.close()



if __name__ == '__main__':

    pass_val = 0

    voidgals_filename = '.build/plots/elaphro_rad_2.0/voidgals_infall.dat'

    N_gals, frac_in_void, rel_dist, infall_amp, infall_tau = (
        voidgals_props_read_data(voidgals_filename) )

    voidgals_props_plot(N_gals, frac_in_void, rel_dist, infall_amp, infall_tau, galaxy_size)

    print("pass_val = ",pass_val)

    sys.exit(pass_val)
