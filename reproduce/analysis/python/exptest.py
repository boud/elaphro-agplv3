# exptest: Non-linear least-squares fit a decaying exponential to infall rate
# (C) 2019-2020 Boud Roukema, Marius Peper, Ahsan Nazer GPL-3+

# The present strategy of this routine is to first do a least-squares
# linear fit to the log infall rate versus time, and then use this
# first solution as an initial guess for a non-linear least-squares
# fit to the original data, starting from the first non-zero datum,
# and then allowing zero values as part of the data to be fit.
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


import numpy as np
import matplotlib
matplotlib.use('Agg')
from scipy.optimize import curve_fit, OptimizeWarning
import matplotlib.pyplot as plt

from setuptools import version
if (version.pkg_resources.parse_version(np.__version__) >= version.pkg_resources.parse_version("1.17.0")):
    np_modern = True
    # as recommended at https://numpy.org/devdocs/reference/random/index.html
    from numpy.random import default_rng
else:
    np_modern = False

import sys

def exponential_model(t,a,b):
    """
    Exponential model for the non-linear least-squares curve_fit algorithm
    """
    return a*np.exp(-b*t)

def linear_model(t,a,b):
    """
    Linear model for the non-linear least-squares curve_fit algorithm
    """
    return a+(b*t)

def infall_fit(infilename="", amp=1., tau=3., sig=1.0,
               errorFile="log.err", do_plot=False,
               verbose = False):
    """
    Use scipy.optimize.curve_fit to fit  A*exp(-B*t)
    to infall rate data.

    Parameters
    ----------
    infilename: string (optinal)
        File name containing infall rate data

    amp: float
        Amplitude

    tau: float
        Reciprocral of decay rate B

    verbose: boolean
        Print lots of diagnostic info if enabled

    returns
    -------
        popt : array([A,B])
            np.array([A, B]) minimizes the squared residuals of
            (A*exp(-B*t) - infall_rate_data)
    """
    if(np_modern):
        rng = default_rng()

    do_unused_fits = False

    #in python empty string evaluates to False and non empty to True
    if infilename:
        if(verbose):
            print("Will read infilename=",infilename)
        input_data = np.loadtxt(infilename,
                                skiprows=1)
        if(verbose):
            print("input_data=",input_data)
        if(1 == input_data.ndim):
            input_data = np.array([input_data]) # we want an array of arrays
        N_rows = (input_data.shape)[0]
        if(verbose):
            print("rows = ",N_rows)
        t = input_data[1:N_rows:2,0]
        data = input_data[1:N_rows:2,1]
        N_t = t.size
        if(verbose):
            print("N_t = ",N_t," check: t.size, data.size = ",t.size,data.size)
            print("t = ",t)
            print("data = ",data)
        # naive error - same everywhere
        error = np.std(data) * np.ones(N_t)
    else:
        ## test case only!
        N_t = 10
        t = np.linspace(1,N_t,N_t)

        decay_rate = 1/tau
        data = exponential_model(t, amp, decay_rate)

        ## Overwrite the first few values with zeroes
        data[0:3] = 0.0

        if(np_modern):
            error = sig*(data*rng.standard_normal(size=data.size))
        else:
            error = sig*(data*np.random.normal(size=data.size))
            data = data+error
            #t=infall_data[:,0]
            #data=infall_data[:,1]

    # Create natural log forms of the data for values where it is non-zero
    w=np.where(data>0) # where is the data positive?
    if(verbose):
        print("N_t = ",N_t," of which there are ",len(data[w])," nonzero elements.")

    # linear positive
    data_nonzero=data[w]
    t_nonzero=t[w]
    # ln positive
    #logt=np.log(t[w]) # not used
    # Use the natural log internally; convert to log10 for the return values
    logdata=np.log(data[w])
    abserror_nonzero = abs(error[w])

    if(verbose):
        print("t_nonzero = ",t_nonzero)
        print("logdata = ", logdata)

    if(logdata.size <= 1):
        print("Warning for %s" % infilename)
        print("Not enough values which are not zero\n")
        popt = [ 0.0,0.0 ]
        return np.array(popt)

    ### Obsolete: make a semilog guess:
    ## find the decay time scale between the first two points
    ## use the result as initial guess
    #bsemilog = -(logdata[1]-logdata[0])/(t_nonzero[1]-t_nonzero[0])
    ## take the geometric mean of the amplitude from the first two points
    #ampsemilog = np.sqrt( (data_nonzero[1]*np.exp(bsemilog*t_nonzero[1])) *
    #                      (data_nonzero[0]*np.exp(bsemilog*t_nonzero[0])))
    #print("ampsemilog, bsemilog = ",ampsemilog,bsemilog)

    if(verbose):
        print("t_nonzero.size =", t_nonzero.size)
        print("logdata.size =", logdata.size)
    #semilogpopt, semilogpcov = curve_fit(exponential_model, t_nonzero, data_nonzero, p0=[logpopt[0],-logpopt[1]],sigma=abserror_nonzero)

    ## First find linear fit to semilog data (ln(dM/dt) vs t)
    semilogpopt, semilogpcov = curve_fit(linear_model, t_nonzero, logdata)
    if(verbose):
        print("semilogpopt, semilogpcov = ",semilogpopt, semilogpcov)
    if(do_plot):
        plt.plot(t, exponential_model(t,np.exp(semilogpopt[0]),-semilogpopt[1]),'b+', label="semilog")

    if(do_unused_fits):
        ## Do non-linear fit result and print result. Not used further.
        # Cheat by using the absolute value of the known uncertainties.
        try:
            ipopt,ipcov = curve_fit(exponential_model, t, data, sigma=abs(error))
            if(do_plot):
                plt.plot(t, exponential_model(t,*ipopt),'r-', label="non-linear",)
        except RuntimeError as e:
            print("Unused curve_fit failed; continuing.")
            print(e.args)

        print("ipopt=",ipopt)

    ## Use semilog fit as initial guess for non-linear exponential fit of all data, including zeroes:
    tol = 1e-100
    # Find index of first positive (greater than tol) data value
    index0 = ((np.array(np.where(data > tol)))[0])[0]
    # print('index0=', index0) # debug

    ## Create indices for fitting that start with the first non-zero data value
    indices = np.arange(index0, N_t)
    # print('indices = ',indices) # debug

    try:
        popt,pcov = curve_fit(exponential_model, t[indices], data[indices],
                              p0=[np.exp(semilogpopt[0]),-semilogpopt[1]],maxfev=6000)
    except RuntimeError as e:
        #curve_fit fails so we set popt = [0., 0.]
        print("curve_fit failed, returning [0,0] and continuing!")
        popt = [0., 0.]
        if infilename:
            errorFile = infilename+".err"
            with open(errorFile, 'a') as f:
                print(e.args,file=f)

    except OptimizeWarning as e:
        #only catch optimization warning when non-linear least squares succeedes but fails
        #to find the covariance matrix. We are only interested in popt
        print(e.args)
        print("Still continuing!")

    if(verbose):
        print("popt=",popt)
    if(do_plot):
        plt.plot(t, exponential_model(t,*popt), 'g-', label="non-linear with clue")
        plt.scatter(t,data)
        plt.xlabel(r'$t$')
        plt.ylabel(r'$y$')

        plt.legend(loc='upper right', numpoints=1, labelspacing=0.1)

        if infilename:
            filenumber=infilename.strip(",.abcdefghijklmnopqrstuvwxyz/_")
            print("filenumber=",filenumber)
            plt.savefig("exp_test"+filenumber+".eps")
        else:
            plt.savefig("exp_test"+".eps")

        plt.close()

    # Avoid dividing by zero; cases with popt[1] = zero will be
    # given 1/tol as their time scales.
    tol = 1e-40
    if(np.abs(popt[1]) > tol):
        tau = 1/popt[1]
    else:
        tau = 1/tol

    return (popt[0], tau)

if __name__ == '__main__':

    if (len(sys.argv) > 1):
        infilename = sys.argv[1]
    else:
        infilename = ""

    pass_value = 0
    fail_flag = 1
    tol = 1e-5

    amp_list = np.array([1.0, 1e-5, 1000.0])
    tau_list = np.array([3.0, 0.07, 27.9])
    for amp in amp_list:
        for tau in tau_list:
            ampfit, taufit = infall_fit(infilename, amp=amp, tau=tau)
            print('amp, tau, ampfit, taufit = ',
                  amp, tau, ampfit, taufit)
            if( (np.abs(ampfit-amp)/amp > tol) or
                (np.abs(taufit-tau)/tau > tol) ):
                pass_value += fail_flag
                print("exptest Error!")
            fail_flag *= 2 # shift the flag to the next bit
            print("\n\n")


    print("pass_value = ",pass_value)

    sys.exit(pass_value)
