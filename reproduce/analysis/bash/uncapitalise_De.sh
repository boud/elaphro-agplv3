#!/usr/bin/env bash

# GPL 3.0 or later Boud Roukema (2015, 2020)
# WARNING this is a crude hack. This is not a systematic solution - a better
# solution would be to understand .bst files

LANG=C

if [ -e "${1}-de.tex" ]; then
    printf "uncapitalise_De.sh: Please remove ${1}-de.tex before running this script.\n"
    exit 1
fi
sed -e 's/{De\([ \n~]\)/{de\1/g' ${1}.tex |
    sed -e 's/{Van\([ \n~]\)/{van\1/g' \
    > ${1}-de.tex
