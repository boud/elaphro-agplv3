#!/bin/bash

## Quick plot of infall rates depending on void/non-void location
format=X

for select_voids in 0 1; do
    for f in $(ls .build/plots/infall_rate_[1-9]*.dat); do
        len=$(wc -l <$f);
        lenm1=$(echo $((${len}-1)));
        void=$(tail -n ${lenm1} $f |awk -v lenm1=${lenm1} '{v+=$4} END {print v/lenm1}');
        title=final_halo_$(basename $f|sed -e 's/infall_rate_//' -e 's/000000000\.dat//');
        if [ "x${void}" = "x${select_voids}" ]; then
            (tail -n${lenm1} $f |awk '{print $1/1000,$2}'; printf "\n");
        fi;
    done \
        |graph -T${format} -ly -X "t (Gyr)" -Y "infall rate (Msun/\*Dt)" -y 3e6 3e10 -L"voids=${select_voids}";
done 
