#!/usr/bin/env bash

# verify-parameter-statistically
# Copyright (C) 2020 Boud Roukema, GPL-3+
#
# Statistically verify a floating-point parameter against a precalculated
# set of realisations, as an alternative to byte-for-byte verification.
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


## verify-parameter-statistically:
##
## Shell script to decide if a calculated value agrees with a precalculated
## value and precalculated error to within a certain factor of that
## error. The factor is loosely termed "sigma", but the statistical meaning
## depends on the statistical meaning of the precalculated error, which may
## vary depending on the chosen usage. If the distribution of the parameter
## is non-Gaussian, e.g. if it has strong skew, then the "number of sigmas"
## for testing for agreement ("verification") will have to be made much
## higher than would make sense for a Gaussian distribution.


## INITIALISATION: See 'INITIALISATION' below for instructions on how
## to set up a set of realisations for using this script.


#1: name of calculated value to check
#
#2: name of uncertainty in the value to check
#
#3: factor of uncertainty allowed in deviation from the canonical value
#   (number of Gaussian 'sigmas'; set this to a huge value like 100.0 to
#   guarantee 'verification'; set it to a tiny value like 0.1 to almost
#   guarantee 'failure'; a non-Gaussian distribution requires a high enough
#   'Gaussian' number of sigmas to compensate for the non-Gaussianity; see
#   INITIALISATION below for more discussion)
#
#4: file with pre-calculated canonical values and uncertainties
#   - if cosmic variance is used, then the realisation files
#     must be in the same directory
#
#5: file with freshly calculated values and uncertainties
#
#6: If a hash that is not the one-character string "0" is given, cosmic
#     variance will be calculated from early results and used instead of
#     the canonical uncertainty; otherwise, the canonical uncertainty will
#     be used.
#   Set this to the one-character string "0" to disable it (quotes will
#   generally not be needed on the shell command line).
#
#7: [2-9] [optional] number of decimal places for writing the cosmic
#     variance; only used if 6 is enabled
#
#8: Temporary file for adding verification comments to LaTeX file target.
#
#
#9: Value of the make variable 'verify-outputs'. Failed verifications
#   give exit values of 1 only if verify-outputs is set to 'yes'.


# Avoid FR/PL decimal comma!
LANG=C
LC_NUMERIC=C

# Enable these for debugging.
set +e
set +x


  # Give useful names to the parameters
  paramname=$1
  paramerrorname=$2
  paramNsig=$3
  canonicalfile=$4
  verifydir=$(dirname ${canonicalfile})
  checkfile=$5
  commit_ID=$6
  
  printf "Initial checking: ${paramname}; ${paramerrorname}; ${paramNsig}; ${canonicalfile}; ${checkfile}\n"
  # This is the canonical value - a supposedly true value calculated
  # earlier, typically by the authors of the research paper, in a
  # realisation that they believe to be correct.
  paramcanon=$(grep "${paramname}" ${canonicalfile} | \
                      awk -F '{' '{print $3}' | tr -d '}')
  # This is the value from this particular realisation. It has to
  # be compared to the canonical value.
  paramcheck=$(grep "${paramname}" ${checkfile} | \
                      awk -F '{' '{print $3}' | tr -d '}')
  param_error_backup=$(grep "${paramerrorname}" ${canonicalfile} | \
                              awk -F '{' '{print $3}' | tr -d '}')
  # Empty the paramerror variable, because we will see if a cosmic variance
  # is requested and available.
  paramerror=
  cosmic_variance=

  # If the commit ID is not the string '0' then read the old realisation data and calculate statistics
  if [ "x${commit_ID}" != "x0" ]; then
      # Find all instances of 'paramname' in the old realisation data files
      # and store them as a space-separated string 'parvaluelist'.
      parvaluelist=$(grep "%%.*VERIFIED.*${paramname}" ${verifydir}/commit_${commit_ID}_*verify.tex | \
                            sed -e 's/=/ /g' | awk '{print $6}' |tr '\n' ' ')
      # If the list of old values is non-empty, the calculate a standard deviation 'paramerror'.
      if [ "x${parvaluelist}" != "x" ]; then \
          paramerror=$(echo ${parvaluelist} | tr ' ' '\n' | \
                              awk '{c+=1; m+=$1; s+=$1*$1} END {m/=c; sd0=sqrt(s/c - m*m); sd=((c>1)? sd0*sqrt(c/(c-1.0)) : sd0); print sd}')
      fi
      # If paramerror is non-empty, then a cosmic variance (systematic
      # error) calculation will be possible. Use the mean of this set of
      # values to calculate a new canonical value that overrides the
      # canonical value that was originally calculated in a single
      # realisation.
      if [ "x${paramerror}" != "x" ]; then
          cosmic_variance="yes"
          paramcanon=$(echo ${parvaluelist} | tr ' ' '\n' | \
                              awk '{c+=1; m+=$1} END {m/=c; print m}')
          # Rewrite paramerror with the requested number of decimal places
          # into 'paramerrorprecision'.
          paramerrorprecision=$(echo ${paramerror} | awk '{printf "%."'$7'"f", $1}')
          # Write the LaTeX macro to the temporary verification LaTeX macro file.
          printf "\\\\newcommand{\\\\${paramname}CosmStdDev}{${paramerrorprecision}}\n" >> $8
      fi
  fi

  # If cosmic variance (from multiple realisations) is not available, then
  # use the canonical file for the error estimate.
  #
  # WARNING: a hardwired lower limit of 1e-10 is used to avoid division by zero.
  # TODO: reduce the chance of user errors related to this.
  if [ "x${cosmic_variance}" != "xyes" ]; then
      paramerror=$(printf " ${param_error_backup}" | awk '{if($1 > 0)print $1; else print 1e-10}')
  fi

  # If the cosmic variance is not used, then we are checking the difference between
  # estimate from two individual realisations. For Gaussian distributions, the
  # error distribution in the difference is \sqrt{2} times the standard deviation
  # of the error for the parameter on its own. We can handle this in either the
  # error or the number of sigmas. Here we choose the "number of sigmas".
  if [ "x${cosmic_variance}" != "xyes" ]; then
      paramNsig=$(printf " ${paramNsig}" | awk '{print $1*sqrt(2)}')
  fi


  # Verification step.
  # Evaluate:
  #   delta = abs(paramcheck - paramcanon) / paramerror
  #
  # If delta < paramNsig, then the new realisation agrees with the original
  # single realisation mean and error (if commit_ID is '0'), or with the
  # mean and error of the ensemble of realisations (if commit_ID is not '0').

  ## WARNING: The leading space in the printf command in the evaluation of
  ## passfail must *not* be removed. This space prevents '-', in the
  ## case that ${paramcanon} is negative, from being interpreted as a
  ## special character.
  
  printf "Checking: paramname=${paramname} paramcanon=${paramcanon} paramcheck=${paramcheck} paramerror=${paramerror} paramNsig=${paramNsig}\n"
  passfail=$(LC_NUMERIC=C LANG=C printf " ${paramcanon} ${paramcheck} ${paramerror} ${paramNsig}" | \
                    awk '{delta=($1 < $2)? ($2-$1)/$3 : ($1-$2)/$3; if(delta < $4) print 0; else print 1}')
  
  if [ "x${passfail}" = "x0" ]; then
    printf "Verified: checked that ${paramname}=${paramcheck} agrees with ${paramcanon} +- ${paramerror} to within ${paramNsig} sigma.\n"
    printf "%%%% (VERIFIED) checked that ${paramname}=${paramcheck} agrees with ${paramcanon} +- ${paramerror} to within ${paramNsig} sigma.\n" >> $8
  else
    printf "Failed verification: ${paramname}=${paramcheck} disagrees with ${paramcanon} +- ${paramerror} to at least ${paramNsig} sigma.\n"
    printf "%%%% FAILED verification: ${paramname}=${paramcheck} disagrees with ${paramcanon} +- ${paramerror} to at least ${paramNsig} sigma.\n" >> $8
    if [ x"$9" = xyes ]; then exit 1; fi
  fi



## INITIALISATION
## 
## To create a set of 'statistically verifiable' values, you
## need to do something along the lines of the following steps.
## DISCLAIMER: As of 1 December 2020, this method is still 
## novel and these instructions are likely to need further
## clarification. Please add an 'issue' at
##
## https://codeberg.org/boud/elaphrocentre
## 
## to propose improvements or ask for clarification.

## 1. This script assume that the only parameters you wish
## to statistically verify are those which are output
## as LaTeX macros in '.build/tex/macros/analyse-plot.tex'
## by the 'make' script 'reproduce/analysis/make/analyse-plot.mk'.
##
## Warning/TODO: The current assumption is that 'grep' on the
## parameter names on '.build/tex/macros/analyse-plot.tex'
## will yield unique values. For example, having both
## GalaxyMass and MeanMassGalaxy there is no ambiguity;
## but GalaxyMass and GalaxyMassMean would both match
## the string 'GalaxyMass' and may result in errors. If this
## flaw has been corrected, then please remove this disclaimer.
## [unique greppable names warning]

## 2. In the configuration file
## 'reproduce/analysis/config/verify-outputs.conf',
## set n_verify_sigmas to a ridiculously high value,
## set the commit ID to 0, and disable verify-outputs:
##
## cosmic_var_commit_ID =
## n_verify_sigmas = 1000.0
## verify-outputs = 

## 3. Remove the existing 'analyse-plot.tex' file:
## 'rm .build/tex/macros/analyse-plot.tex'

## 4. Generate a new analyse-plot.tex file
##
## './project make'
##
## This might have errors if you have already started
## using verification values in your paper.tex file.
## This should only be a temporary problem.

## 5. Check the new '.build/tex/macros/analyse-plot.tex' file.
## 
## If this looks correct, then copy it to
##
## 'reproduce/analysis/verify/analyse-plot.tex.verify'
## 
## This file will be considered your 'canonical' file
## with the expected values of parameters and the
## internal (random) error estimates calculated by whatever
## method you choose (e.g. a bootstrap method, or sqrt{N} for
## a Poisson type statistic) from within your single realisation.

## 6. Enable the verify-outputs in
##
## 'reproduce/analysis/config/verify-outputs.conf'
##
## verify-outputs = yes
    
## 7. Generate your first set of verification values.
## 
## './project make'
##
## The LaTeXing step may fail, but you should obtain your first
## verification file
## 
## '.build/tex/macros/verify.tex'
##
## Copy this file to the verify/ directory and label it with your commit
## name and some other information to distinguish it from future
## realisations, e.g.
##
## reproduce/analysis/verify/commit_1234567_run01_verify.tex

## 8. A sample standard deviation needs, at the bare minimum, a sample size
## of two. Run a *new*, statistically independent realisation of your whole
## project. For example, these commands may work in a typical
## Maneage setup:
##
## ./project make clean
## ./project make

## 9. (similar to step 7). You should now have a new
## verification file
## 
## '.build/tex/macros/verify.tex'
##
## Copy this file and label it with your commit name and a realisation
## identifier, e.g.
##
## reproduce/analysis/verify/commit_1234567_run02_verify.tex

    
## 10. If everything seems correct, then continue to generate a fair
## sample of realisations, creating a set of files such as
##
## reproduce/analysis/verify/commit_1234567_run*_verify.tex
##

## 11. Now:
##
## 11.1 reduce the number of "sigmas" to a reasonable value
## such as
##
## n_verify_sigmas = 4.0
##
## 11.2 and add the commit ID
##
## cosmic_var_commit_ID = 1234567
##
## in 'reproduce/analysis/config/verify-outputs.conf'
##
## 11.3 and one or two calls to this script in 'verify.mk'
##
## 11.4 and do './project make' to see if your current run is
## statistically consistent with the first realisation
## now stored in 'analyse-plot.tex.verify', given the rules
## that you have set up in 'verify.mk'.
##
## You can start out using the internal (random) errors
## of the first run; then shift to using the external (systematic)
## errors from the ensemble of realisations. For highly skew or
## other non-gaussian distributions, you may need to increase
## n_verify_sigmas a lot.
     
