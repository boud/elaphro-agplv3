# Verify the project outputs before building the paper.
#
# Copyright (C) 2020-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.


# Abbreviation for statistical verification script.
verify_parameter_statistically = $(bashdir)/verify-parameter-statistically.sh



# Verification functions
# ----------------------
#
# These functions are used by the final rule in this Makefile
verify-print-error-start = \
  echo; \
  echo "VERIFICATION ERROR"; \
  echo "------------------"; \
  echo

verify-print-tips = \
  echo "If you are still developing your project, you can disable"; \
  echo "verification by removing the value of the variable in the"; \
  echo "following file (from the top project source directory):"; \
  echo "    reproduce/analysis/config/verify-outputs.conf"; \
  echo; \
  echo "If this is the final version of the file, you can just copy"; \
  echo "and paste the calculated checksum (above) for the file in"; \
  echo "the following project source file:"; \
  echo "    reproduce/analysis/make/verify.mk"

# Removes following components of a plain-text file, calculates checksum
# and compares with given checksum:
#   - All commented lines (starting with '#') are removed.
#   - All empty lines are removed.
#   - All space-characters in remaining lines are removed (so the width of
#     the printed columns won't invalidate the verification).
#
# It takes three arguments:
#   - First argument: Full address of file to check.
#   - Second argument: Expected checksum of the file to check.
#   - File name to write result.
verify-txt-no-comments-no-space = \
  infile=$(strip $(1)); \
  inchecksum=$(strip $(2)); \
  innobdir=$$(echo $$infile | sed -e's|$(BDIR)/||g'); \
  if ! [ -f "$$infile" ]; then \
    $(call verify-print-error-start); \
    echo "The following file (that should be verified) doesn't exist:"; \
    echo "    $$infile"; \
    echo; exit 1; \
  fi; \
  checksum=$$(sed -e 's/[[:space:]][[:space:]]*//g' \
                  -e 's/\#.*$$//' \
                  -e '/^$$/d' $$infile \
                  | md5sum \
                  | awk '{print $$1}'); \
  if [ x"$$inchecksum" = x"$$checksum" ]; then \
    echo "%% (VERIFIED) $$checksum $$innobdir" >> $(3); \
  else \
    $(call verify-print-error-start); \
    $(call verify-print-tips); \
    echo; \
    echo "Checked file (without empty or commented lines):"; \
    echo "    $$infile"; \
    echo "Expected MD5 checksum:   $$inchecksum"; \
    echo "Calculated MD5 checksum: $$checksum"; \
    echo; exit 1; \
  fi;


count-verify-files = $$(ls $(verifydir)/commit_$(strip $(1))_*verify.tex | wc -l)


# Final verification TeX macro (can be empty)
# -------------------------------------------
#
# This is the FINAL analysis step (before going onto the paper. Please use
# this step to verify the contents of the figures/tables used in the paper
# and the LaTeX macros generated from all your processing. It should depend
# on all the LaTeX macro files that are generated (their contents will be
# checked), and any files that go into the tables/figures of the paper
# (generated in various stages of the analysis.
#
# Since each analysis step's data files are already prerequisites of their
# respective TeX macro file, its enough for `verify.tex' to depend on the
# final TeX macro.
#
# USEFUL TIP: during the early phases of your research (when you are
# developing your analysis, and the values aren't final), you can comment
# the respective lines.
#
# Here is a description of the variables defined here.
#
#   verify-dep: The major step dependencies of `verify.tex', this includes
#               all the steps that must be finished before it.
#
#   verify-changes: The files whose contents are important. This is
#               essentially the same as `verify-dep', but it has removed
#               the `initialize' step (which is information about the
#               pipeline, not the results).
verify-dep = $(subst verify,,$(subst paper,,$(makesrc)))
verify-check0 = $(subst initialize,,$(verify-dep))
verify-check1 = $(subst download,,$(verify-check0))
verify-check2= $(subst create-trees,,$(verify-check1))
verify-check = $(subst analyse-plot,,$(verify-check2))

$(mtexdir)/verify.tex: $(foreach s, $(verify-dep), $(mtexdir)/$(s).tex)

        # Make sure that verification is actually requested.
        #printf "verify-check=$(verify-check)\n"
	LC_NUMERIC=C
	LANG=C
	if [ x"$(verify-outputs)" = xyes ]; then

          # Make sure the temporary output doesn't exist (because we want
          # to append to it). We are making a temporary output target so if
          # there is a crash in the middle, Make will not continue. If we
          # write in the final target progressively, the file will exist,
          # and its date will be more recent than all prerequisites, so
          # next time the project is run, Make will continue and ignore the
          # rest of the checks.
	  rm -f $@.tmp

          # Verify TeX macros (the values that go into the PDF text).
	  for m in $(verify-check); do
	    file=$(mtexdir)/$$m.tex
	    if [ $$m == init-N-body ]; then
	       s=fbbe65666167b5220cd9813561d7696b
	       egrep -v "NCPUSvalue|Seedvalue" $${file} > $${file}.verify
	    elif [ $$m == run-simulations ]; then
	       s=b58ac4acba4bc786dbb80c4362018468
	       cat $${file} > $${file}.verify
	    elif [ $$m == detect-haloes ]; then
	       s=4cf038b9fd377bbc0916fb58e2b0ad1c
	       cat $${file} > $${file}.verify
	    elif [ $$m == gal-recipes ]; then
	       s=cd176141164e75228a1aecd33616be1b
	       cat $${file} > $${file}.verify
	    elif [ $$m == detect-voids ]; then
	       s=b6b8b28b31455af6b6ec13b985bff3a1
	       cat $${file} > $${file}.verify
	    else echo; echo "'$$m' not recognized."; exit 1
	    fi
	    $(call verify-txt-no-comments-leading-space, $${file}.verify, $$s, $@.tmp)
	  done

	else
	  echo "% Verification was DISABLED!" > $@.tmp
	fi


	if [ x"$(verify-outputs)" = xyes ]; then
          # Verify the best values of the tables (the values that go into the PDF text).
	  # We leave verification of the uncertainties to an open question not handled here.

	  # Elaphrocentre: Verify rules for global population
	  # parameters: infall history (4), galaxy scale length and
	  # associated parameters (12), elaphroacceleration (2), and
	  # slopes of relations in the figures for which we provide
	  # these (13). An allowance of 4 sigma is given by
	  # default. Two parameters that are known to be somewhat
	  # sensitive to cosmic variance are allowed 8 sigma
	  # tolerance.

	  # Infall histories table
	  $(verify_parameter_statistically) \
	          InfallMedianValueAmplitudeVoidsvalue \
	          InfallRobustErrorAmplitudeVoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          InfallMedianValueAmplitudeNonVoidsvalue \
	          InfallRobustErrorAmplitudeNonVoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          InfallMedianValueDecayVoidsvalue \
	          InfallRobustErrorDecayVoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          InfallMedianValueDecayNonVoidsvalue \
	          InfallRobustErrorDecayNonVoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)

	  # Galaxy size table - restricted mass interval
	  $(verify_parameter_statistically) \
	          PlotsSizeiRAvgVoidsvalue \
	          PlotsSizeiErrorRAvgVoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          PlotsSizeiRAvgNonvoidsvalue \
	          PlotsSizeiErrorRAvgNonvoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          SpiniSpinAvgVoidsvalue \
	          SpiniErrorSpinAvgVoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          SpiniSpinAvgNonvoidsvalue \
	          SpiniErrorSpinAvgNonvoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          RviriRvirAvgVoidsvalue \
	          RviriErrorRvirAvgVoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          RviriRvirAvgNonvoidsvalue \
	          RviriErrorRvirAvgNonvoidsvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)

	  # Galaxy size table - full mass range
	  $(verify_parameter_statistically) \
	          PlotsSizeiRAvgVoidsTotalvalue \
	          PlotsSizeiErrorRAvgVoidsTotalvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          PlotsSizeiRAvgNonvoidsTotalvalue \
	          PlotsSizeiErrorRAvgNonvoidsTotalvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          SpiniSpinAvgVoidsTotalvalue \
	          SpiniErrorSpinAvgVoidsTotalvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          SpiniSpinAvgNonvoidsTotalvalue \
	          SpiniErrorSpinAvgNonvoidsTotalvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          RviriRvirAvgVoidsTotalvalue \
	          RviriErrorRvirAvgVoidsTotalvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          RviriRvirAvgNonvoidsTotalvalue \
	          RviriErrorRvirAvgNonvoidsTotalvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)

	  # Elaphro-acceleration table
	  $(verify_parameter_statistically) \
	          ElaphroAcciAccRadMedvalue \
	          ElaphroAcciAccRadStderrMedvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  ## the extremely high "sigma" is a cosmic variance estimate:
	  $(verify_parameter_statistically) \
	          ElaphroAcciAccTanMedvalue \
	          ElaphroAcciAccTanStderrMedvalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 2 $@.tmp $(verify-outputs)

	  # Infall vs haloinvoidfrac figures
	  # this parameter has some cosmic variance:
	  $(verify_parameter_statistically) \
	          InfallAmpFracSlopevalue \
	          InfallAmpFracSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          InfallTauFracSlopevalue \
	          InfallTauFracSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 2 $@.tmp $(verify-outputs)

	  # Infall reldist figures
	  $(verify_parameter_statistically) \
	          InfallAmpReldistSlopevalue \
	          InfallAmpReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          InfallTauReldistSlopevalue \
	          InfallTauReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)

	  # Galaxy scale length figures
	  #  - there is modest cosmic variance for this value:
	  $(verify_parameter_statistically) \
	          GalsizeFracSlopevalue \
	          GalsizeFracSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          ElaphcenGalsizeReldistSlopevalue \
	          ElaphcenGalsizeReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  if [ x"$(strip $(compare_centres))" = xYES ]; then
	    $(verify_parameter_statistically) \
	          CirccenGalsizeReldistSlopevalue \
	          CirccenGalsizeReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	    $(verify_parameter_statistically) \
	          GeomcenGalsizeReldistSlopevalue \
	          GeomcenGalsizeReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  fi

	  # Galaxy spin parameter
	  $(verify_parameter_statistically) \
	          GalspinFracSlopevalue \
	          GalspinFracSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 3 $@.tmp $(verify-outputs)
	  # - there is modest cosmic variance for this value:
	  $(verify_parameter_statistically) \
	          ElaphcenGalspinReldistSlopevalue \
	          ElaphcenGalspinReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 3 $@.tmp $(verify-outputs)
	  if [ x"$(strip $(compare_centres))" = xYES ]; then
	    $(verify_parameter_statistically) \
	          CirccenGalspinReldistSlopevalue \
	          CirccenGalspinReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 3 $@.tmp $(verify-outputs)
	    $(verify_parameter_statistically) \
	          GeomcenGalspinReldistSlopevalue \
	          GeomcenGalspinReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 3 $@.tmp $(verify-outputs)
	  fi

	  # Galaxy host halo virial radius
	  $(verify_parameter_statistically) \
	          GalRvirFracSlopevalue \
	          GalRvirFracSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex 0 2 $@.tmp $(verify-outputs)
	  $(verify_parameter_statistically) \
	          ElaphcenGalRvirReldistSlopevalue \
	          ElaphcenGalRvirReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 1 $@.tmp $(verify-outputs)
	  if [ x"$(strip $(compare_centres))" = xYES ]; then
	    $(verify_parameter_statistically) \
	          CirccenGalRvirReldistSlopevalue \
	          CirccenGalRvirReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 1 $@.tmp $(verify-outputs)
	    $(verify_parameter_statistically) \
	          GeomcenGalRvirReldistSlopevalue \
	          GeomcenGalRvirReldistSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 1 $@.tmp $(verify-outputs)
	  fi

	  # Elaphro-acceleration
	  $(verify_parameter_statistically) \
	          AccRadSlopevalue \
	          AccRadSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 3 $@.tmp $(verify-outputs)
	  # - there is modest cosmic variance for this value:
	  $(verify_parameter_statistically) \
	          AccTanSlopevalue \
	          AccTanSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 3 $@.tmp $(verify-outputs)

	  # Void parameter relation: haloinvoidfrac vs r/R_{eff}
	  # - there is modest cosmic variance for this value:
	  $(verify_parameter_statistically) \
	          FracRreffSlopevalue \
	          FracRreffSigSlopevalue \
	          $(n_verify_sigmas) \
	          $(verifydir)/analyse-plot.tex.verify \
	          $(mtexdir)/analyse-plot.tex $(cosmic_var_commit_ID) 3 $@.tmp $(verify-outputs)

	## Add LaTeX macros to quantify the verification criteria:
	  printf "\\\\newcommand{\\\\CosmicVarCommitID}{{\\\\tt $(cosmic_var_commit_ID)}}\n" >> $@.tmp
	  printf "\\\\newcommand{\\\\NVerifySigmas}{$(n_verify_sigmas)}\n" >> $@.tmp
	  # TODO: Handle the startup case when $(cosmic_var_commit_ID) is empty, for generating new standard cases.
	  count_verify_files=$(call count-verify-files, $(cosmic_var_commit_ID))
	  printf "\\\\newcommand{\\\\NVerifyFiles}{$${count_verify_files}}\n" >> $@.tmp

	else
	  echo "% Verification was DISABLED; the LaTeX paper will fail due to missing values!" > $@.tmp
	fi


        # Move temporary file to final target.
	mv $@.tmp $@
