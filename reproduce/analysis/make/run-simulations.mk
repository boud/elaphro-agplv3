# Run the simulations.
#
# (C) 2020 Boud Roukema, Marius Peper, GPL-3+
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


#ibdir = $(bsdir)/installed/bin
#init_dir = $(badir)/n-body-init
ramses_dir = $(init_dir)
#sim_paramsfile = $(pconfdir)/Nbody-sim-params.conf
example_sims_output_file = $(init_dir)/output_00001/part_00001.out00001
namelist_output_file = $(ramses_dir)/output_00001/namelist.txt
done_run_simulation_file = $(ramses_dir)/done-run-simulation
status_run_simulation_file = $(ramses_dir)/status-run-simulation

## The following file produced by ramses is deliberately *not* given
## a rule to tell `make` how to create it. The aim is that the
## rules that depend on the existence of this file will fail
## if the file does not exist, but they will *not* automatically
## cause ramses to be re-run. This should avoid heavy new simulations
## that destroy existing simulations.
example_sims_output_file_no_rule = $(init_dir)/output_00001/part_00001.out00002

## Include the initial conditions and RAMSES parameters:
#include ${sim_paramsfile} # included earlier by top-make.mk

RAMSES_part_header_bytes = 200
N_CPUS_RAMSES=$(NCPUS)

# aliases
.PHONY : ramses
ramses: run-simulation

# This is the main rule.
.PHONY : run-simulation
run-simulation: $(example_sims_output_file) $(mtexdir)/run-simulations.tex $(done_init_conditions_file) $(init_dir)/ramses-sanity-check

###$(done_run_simulation_file): $(example_sims_output_file)

$(example_sims_output_file) $(namelist_output_file): $(done_init_conditions_file) $(sim_paramsfile) $(init_dir)/mpgrafic-sanity-check
	if [ "x$(ALLOW_RUN_NEW_SIMULATION)" = "xYES" ] ; then
	   cp -pv $(bashdir)/run-ramses $(installdir)/bin
	   cd $(init_dir)
	   mkdir -pv simulation-out
	   rm -frv output_[0-9]*
	   rm -frv simulation-out
	   mkdir -pv simulation-out
	   printf "wishlist\n" > $(status_run_simulation_file)
	   if (grep "define HAVE_ARGP_H 1" $(installdir)/include/cosmdist_config.h); then have_argp_h=1; else have_argp_h=0 ; fi
	   time_start=$(time_start) time_end=$(time_end) time_step=$(time_step) \
	    HAVE_ARGP_H=$${have_argp_h} \
	    LEVELMAX=$(ramses_levelmax) N_CROOT=$(Ncroot) \
	    LBOX=$(Lbox) N_CPUS_RAMSES=$(N_CPUS_RAMSES) \
	    HUBBLE0=$(Hubble) OMEGA_M=$(OmegaM) OMEGA_L=$(OmegaL) \
	    STATUS_FILE=$(status_run_simulation_file) \
	    NGRIDTOT=$(ngridtot) NPARTTOT=$(nparttot) \
	    run-ramses
	   if $$(grep --silent "success" ${status_run_simulation_file}); then
	      touch $(done_run_simulation_file)
	      printf "\nRAMSES was successfully run. See the new files in $(init_dir)/ and the log file.\n\n"
	   else
	      rm -f $(done_run_simulation_file)
	      printf "\nRAMSES failed. Check the log file (name above).\n\n"
	      exit 1
	   fi
	else
	   printf "If you want to run a new simulation you have to modify reproduce/analysis/config/init-conditions-params.conf.\n"
	   printf "Please set ALLOW_RUN_NEW_SIMULATION = YES to allow to start a new simulation. This is a safety\n"
	   printf "feature to decrease the chance that the pipeline will accidentally remove the previous simulation\n"
	   printf "when that is not intended.\n"
	   printf '\nRAMSES did not run!.\n\n'
	fi

## This rule is aimed at doing a sleep cycle, with a maximum timeout,
## to see if the file with no rule (apart from this one) will eventually
## be created by some other rule (in fact, the one just above). We do
## *not* want this rule to be forced.
$(done_run_simulation_file):
	count=0
	printf "Waiting for done_run_simulation_file=$(done_run_simulation_file)...\n"
	while ( ! [ -e $(done_run_simulation_file) ] && \
	          [ $${count} -lt 0$(max_sleep_seconds) ] && \
	          [ "x$${status_run}" != "xfailed" ] ); do
	   sleep $(sleep_cycle_seconds)
	   count=$$(($${count}+$(sleep_cycle_seconds)))
	   status_run=$$(cat $(status_run_simulation_file))
	   printf "Waiting for simulations to be ready. count=$${count} seconds.\n"
	   done
	if [ -e $(done_run_simulation_file) ]; then
	   printf "Found $(done_run_simulation_file) .\n"
	else
	   if [ "x$${status_run}" != "xfailed" ]; then
	      printf "Ramses failed. Check files in $(ramses_dir).\n"
	      exit 1
	   else
	      printf "Timeout. Did not find $(done_run_simulation_file) .\n"
	      exit 1
	   fi
	fi

# Check the byte counts in the particle output files. This sanity check assumes
# that the RAMSES output routine has not changed.

$(init_dir)/ramses-sanity-check: $(done_run_simulation_file)
	byte_count=$$(wc -c $(init_dir)/output_00001/part_00*.out00*| \
	  tail -n1 | awk '{print $$1}') \
	expected_byte_count=$$(echo $(RAMSES_part_header_bytes) |awk -v N=$(Ncroot) -v ncpu=$(NCPUS) '{print (($$1)*ncpu + 76*N*N*N)}')
	if [ "$${byte_count}" -eq "$${expected_byte_count}" ]; then
	   printf "The files $(init_dir)/output_00001/part_00*.out00* have the expected byte count.\n"
	   touch $(init_dir)/ramses-sanity-check
	else
	   printf "Warning: The files $(init_dir)/output_00001/part_00*.out00* have $${byte_count} bytes but $${expected_byte_count} bytes were expected.\n"
	   rm -f $(init_dir)/ramses-sanity-check
	fi

$(mtexdir)/run-simulations.tex: $(namelist_output_file) $(done_run_simulation_file)
        # This section produces a file with LaTeX macros for some of
        # the parameters in $(namelist_output_file). It presently
        # selects a small hardwired list of parameters rather than all
        # of them.  This section will be re-run if $(namelist_output_file) is
        # updated.
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	param_list="levelmax"
	$(call create_tex_macro, $(namelist_output_file), $${param_list}, int, ramsesparam)
	printf "\\\\newcommand{\\\\HowWeRanSimulations}{This is how we ran the simulations.}\n\n" >> $@
	printf "\nThe LaTeX macro file $(mtexdir)/init-N-body.tex was created.\n\n"

clean-simulation:
	rm -f $(example_sims_output_file) $(init_dir)/ramses-sanity-check \
	  $(namelist_output_file) $(done_run_simulation_file)
