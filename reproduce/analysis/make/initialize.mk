# Project initialization.
#
# Copyright (C) 2018-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2020-2021 Boud Roukema
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.





# High-level directory definitions
# --------------------------------
#
# Basic directories that are used throughout the project.
#
# Locks are used to make sure that an operation is done in series not in
# parallel (even if Make is run in parallel with the `-j' option). The most
# common case is downloads which are better done in series and not in
# parallel. Also, some programs may not be thread-safe, therefore it will
# be necessary to put a lock on them. This project uses the `flock' program
# to achieve this.
#
# To help with modularity and clarity of the build directory (not mixing
# software-environment built-products with products built by the analysis),
# it is recommended to put all your analysis outputs in the 'analysis'
# subdirectory of the top-level build directory.
badir=$(BDIR)/analysis
bsdir=$(BDIR)/software

# Derived directories (the locks directory can be shared with software
# which already has this directory.).
texdir      = $(badir)/tex
lockdir     = $(bsdir)/locks
indir       = $(badir)/inputs
prepdir     = $(padir)/prepare
mtexdir     = $(texdir)/macros
installdir  = $(bsdir)/installed
bashdir     = reproduce/analysis/bash
pconfdir    = reproduce/analysis/config
pythondir   = reproduce/analysis/python
verifydir   = reproduce/analysis/verify
softconfdir = reproduce/software/config
installdir  = $(bsdir)/installed
ilibpydir   = $(bsdir)/installed/lib/python
ibdir       = $(bsdir)/installed/bin

## Directories+files for galaxy formation pipeline.
init_dir = $(badir)/n-body-init
haloes_dir = $(badir)/haloes
tree_dir = $(badir)/mergertrees
mergertree_output_file = $(tree_dir)/trees/tree_0_0_0.dat
gal_dir = $(badir)/galaxies



# Preparation phase
# -----------------
#
# This Makefile is loaded both for the `prepare' phase and the `make'
# phase. But the preparation files should be dealt with differently
# (depending on the phase). In the `prepare' phase, the main directory
# should be created, and in the `make' phase, its contents should be
# loaded.
#
# If you don't need any preparation, please simply comment these lines.
ifeq (x$(project-phase),xprepare)
$(prepdir):; mkdir $@
else
include $(bsdir)/preparation-done.mk
ifeq (x$(include-prepare-results),xyes)
include $(prepdir)/*.mk
endif
endif





# TeX build directory
# ------------------
#
# In scenarios where multiple users are working on the project
# simultaneously, they can't all build the final paper together, there will
# be conflicts! It is possible to manage the working on the analysis, so no
# conflict is caused in that phase, but it would be very slow to only let
# one of the project members to build the paper at every instance
# (independent parts of the paper can be added to it independently). To fix
# this problem, when we are in a group setting, we'll use the user's ID to
# create a separate LaTeX build directory for each user.
#
# The same logic applies to the final paper PDF: each user will create a
# separte final PDF (for example `paper-user1.pdf' and `paper-user2.pdf')
# and no `paper.pdf' will be built. This isn't a problem because
# `initialize.tex' is a .PHONY prerequisite, so the rule to build the final
# paper is always executed (even if it is present and nothing has
# changed). So in terms of over-all efficiency and processing steps, this
# doesn't change anything.
ifeq (x$(GROUP-NAME),x)
texbtopdir  = build
final-paper = paper.pdf
else
user        = $(shell whoami)
texbtopdir  = build-$(user)
final-paper = paper-$(user).pdf
endif
texbdir     = $(texdir)/$(texbtopdir)
tikzdir     = $(texbdir)/tikz





# Original system environment
# ---------------------------
#
# Before defining the local sub-environment here, we'll need to save the
# system's environment for some scenarios (for example after `clean'ing the
# built programs).
curdir   := $(shell echo $$(pwd))





# High level environment
# ----------------------
#
# We want the full recipe to be executed in one call to the shell. Also we
# want Make to run the specific version of Bash that we have installed
# during `./project configure' time.
#
# Regarding the directories, this project builds its major dependencies
# itself and doesn't use the local system's default tools. With these
# environment variables, we are setting it to prefer the software we have
# build here.
#
# `TEXINPUTS': we have to remove all possible user-specified directories to
# avoid conflicts with existing TeX Live solutions. Later (in `paper.mk'),
# we are also going to overwrite `TEXINPUTS' just before `pdflatex'.
.ONESHELL:
.SHELLFLAGS = -ec
export TERM=xterm
export TEXINPUTS :=
export CCACHE_DISABLE := 1
export PATH := $(installdir)/bin
export LDFLAGS := -L$(installdir)/lib
export SHELL := $(installdir)/bin/bash
export CPPFLAGS := -I$(installdir)/include
export LD_LIBRARY_PATH := $(installdir)/lib

# Until we build our own C library, without this, the project's GCC won't
# be able to compile anything if the host C library isn't in a standard
# place: in particular Debian-based operatings sytems. On other systems, it
# will be empty.
export CPATH := $(SYS_CPATH)

# RPATH is automatically written in macOS, so `DYLD_LIBRARY_PATH' is
# ultimately redundant. But on some systems, even having a single value
# causes crashs (see bug #56682). So we'll just give it no value at all.
export DYLD_LIBRARY_PATH :=

# We don't want numerical operations to use a local in which
# the decimal comma is used instead of the decimal point. We will
# assume that non-English-language environments are not needed.
export LANG=C
export LC_NUMERIC=C
export LANGUAGE=en_US:en

# OpenMPI normally uses an existing `ssh' or `rsh' binary to connect
# between different machines with non-shared memory.
#
# For running openmpi on a single machine with multiple cores/threads,
# the 'false' shell should work correctly. This is the current default.
#
# For running openmpi across multiple machines using ssh, you will
# have to modify the default setting here, setting it, most likely,
# to ssh. This assumes you know how to set up ssh keys across
# the machines that you use.
#
# (Since the early 2000s, there has no longer been
# any justification at all for using the old, un-encrypted 'rsh'.)
#
export OMPI_MCA_plm_rsh_agent=false
#export OMPI_MCA_plm_rsh_agent=ssh

# Recipe startup script.
export PROJECT_STATUS := make
export BASH_ENV := $(shell pwd)/reproduce/software/shell/bashrc.sh



# Python enviroment
# -----------------
#
# The main Python environment variable is `PYTHONPATH'. However, so far we
# have found several other Python-related environment variables on some
# systems which might interfere. To be safe, we are removing all their
# values.
export PYTHONPATH             := $(installdir)/lib/python/site-packages
export PYTHONPATH3            := $(PYTHONPATH)
export _LMFILES_              :=
export PYTHONPATH2            :=
export LOADEDMODULES          :=
export MPI_PYTHON_SITEARCH    :=
export MPI_PYTHON2_SITEARCH   :=
export MPI_PYTHON3_SITEARCH   :=





# High-level level directories
# ----------------------------
#
# These are just the top-level directories for all the separate steps. The
# directories (or possible sub-directories) for individual steps will be
# defined and added within their own Makefiles.
#
# The `.SUFFIXES' rule with no prerequisite is defined to eliminate all the
# default implicit rules. The default implicit rules are to do with
# programming (for example converting `.c' files to `.o' files). The
# problem they cause is when you want to debug the make command with `-d'
# option: they add too many extra checks that make it hard to find what you
# are looking for in the outputs.
.SUFFIXES:
$(lockdir): | $(bsdir); mkdir $@



# Version and distribution tarball definitions
project-commit-hash := $(shell if [ -d .git ]; then \
    echo $$(git describe --dirty --always --long | \
    sed -e 's/v[0-9]*-[0-9]*-g//'); else echo NOGIT; fi)
project-committed-only-hash := $(shell if [ -d .git ]; then \
    echo $$(git describe --always --long | \
    sed -e 's/v[0-9]*-[0-9]*-g//'); else echo NOGIT; fi)
project-package-name := $(package-name)-$(project-commit-hash)
project-package-name-committed-only := $(package-name)-$(project-committed-only-hash)
project-package-contents = $(texdir)/$(project-package-name)
project-package-contents-upstream = $(texdir)/$(project-package-name)-upstream




# High-level Makefile management
# ------------------------------
#
# About `.PHONY': these are targets that must be built even if a file with
# their name exists.
#
# Only `$(mtexdir)/initialize.tex' corresponds to a file. This is because
# we want to ensure that the file is always built in every run: it contains
# the project version which may change between two separate runs, even when
# no file actually differs.
.PHONY: all clean dist dist-zip dist-lzip texclean distclean \
        $(project-package-contents) $(mtexdir)/initialize.tex


## Clean all simulations - *including* the N-body simulation! -
## and post-simulation major calculations
clean-calculations: clean-init-conditions clean-simulation \
         clean-haloes clean-mergertree clean-galaxies clean-voids

## Clean all calculations following the N-body simulation:
clean-post-simulation: clean-haloes clean-mergertree clean-galaxies clean-voids

## Run all calculations following the N-body simulation:
create-galaxies-voids: create-galaxies detect-voids


## Same as 'clean-calculations' but also remove the plots
clean-pipeline: clean-calculations clean-analyse-plot


texclean:
	rm -f *.pdf
	rm -rf $(texdir)/build/*
	mkdir $(texdir)/build/tikz # 'tikz' is assumed to already exist.

clean:
        # Delete the top-level PDF file.
	rm -f *.pdf

        # Delete all the built outputs except the dependency
        # programs. We'll use Bash's extended options builtin (`shopt') to
        # enable "extended glob" (for listing of files). It allows extended
        # features like ignoring the listing of a file with `!()' that we
        # are using afterwards.
	shopt -s extglob
	rm -rf $(texdir)/macros/!(dependencies.tex|dependencies-bib.tex|hardware-parameters.tex)
	rm -rf $(badir)/!(tex) $(texdir)/!(macros|$(texbtopdir))
	rm -rf $(texdir)/build/!(tikz) $(texdir)/build/tikz/*
	rm -rf $(bsdir)/preparation-done.mk


distclean: clean
        #  Without cleaning the Git hooks, we won't be able to easily
        #  commit or checkout after this task is done. So we'll remove them
        #  first.
	rm -fv .git/hooks/post-checkout .git/hooks/pre-commit

        # We'll be deleting the built environent programs and just need the
        # `rm' program. So for this recipe, we'll use the host system's
        # `rm', not our own.
	printf "Check the directory name in the following command line, and execute the\n"
	printf "line if you are happy that it is valid.\n\n"
	printf "$$sys_rm -rf $(BDIR)\n" # risky if BDIR was poorly chosen or wrongly evaluated
	$$sys_rm -fv .local .build $(pconfdir)/LOCAL.conf






# Packaging rules
# ---------------
#
# With the rules in this section, you can package the project in a state
# that is ready for building the final PDF with LaTeX. This is useful for
# collaborators who only want to contribute to the text of your project,
# without having to worry about the technicalities of the analysis.
$(project-package-contents): paper.pdf $(texbdir)/paper-full.tex | $(texdir)

        # Set up the output directory, delete it if it exists and remake it
        # to fill with new contents.
	dir=$@
	rm -rf $$dir
	mkdir $$dir

        # Build a small Makefile to help in automatizing the paper building
        # (including the bibliography).
	m=$$dir/Makefile
	echo   "paper.pdf: paper-full.tex"                   > $$m
	printf "\tlatex paper-full && latex paper-full && latex paper-full \\\n" >> $$m
	printf "\t&&dvips paper-full -o paper-tmp.eps \\\n" >> $$m
	printf "\t&&ps2pdf paper-tmp.eps \\\n" >> $$m
	printf "\t&&mv -v paper.pdf $(package-name).pdf\n\n" >> $$m
	echo   ".PHONY: clean"                                   >> $$m
	echo   "clean:"                                          >> $$m
	printf "\trm -f *.aux *.auxlock *.bbl *.bcf\n"           >> $$m
	printf "\trm -f *.blg *.log *.out *.run.xml\n"           >> $$m

        # Copy the top-level contents (see next step for `paper-full.tex').
	cp COPYING project README.md README-hacking.md $$dir/

        # Copy the full `paper-full.tex' into place).
	pwd
	cp -pv $(texbdir)/paper-full.tex $$dir/

        # Build the top-level directories.
	mkdir $$dir/reproduce

        # Copy all the necessary `reproduce' and `tex' contents.
	shopt -s extglob
        #cp -r $$(git ls-files tex/src)           $$dir/tex/src
	pwd
	tar -c -f - $$(git ls-files reproduce) | (cd $$dir ; tar -x -f -); cd $${curdir}
	pwd
        #cp -r tex/build/!($(project-package-name)) $$dir/tex/build
	cp -pv $(texbdir)/*.tex $${dir} # TODO: exclude non-exportable files!!
        # WARNING: hardwired to this specific project:
	mkdir -p $${dir}/fig
	cp -pv $(texbdir)/fig/*.eps $${dir}/fig/ # TODO: exclude non-exportable files!!
        #cp -pv $(texbdir)/*.cl[os] $${dir} # TODO: exclude non-exportable files!!

        # Supplementary data files (generally output files; possibly input files too):
	mkdir $${dir}/anc/
	cp -pv $(data-publish-dir)/* $${dir}/anc/

        # Clean up un-necessary/local files: 1) the $(texdir)/build*
        # directories (when building in a group structure, there will be
        # `build-user1', `build-user2' and etc), are just temporary LaTeX
        # build files and don't have any relevant/hand-written files in
        # them. 2) The `LOCAL.conf' and `gnuastro-local.conf' files just
        # have this machine's local settings and are irrelevant for anyone
        # else.
	rm -rf $$dir/tex/build/build*
        #rm $$dir/reproduce/software/config/LOCAL.conf

        # PROJECT SPECIFIC
        # ----------------
        # Put any project-specific distribution steps here.

        # ----------------

        # Clean temporary files that may have been created by text editors.
	cd $(texdir)
	find $(project-package-name) -name \*~ -delete
	find $(project-package-name) -name \*.swp -delete

# Package into `.tar.gz' or '.tar.lz'.
dist dist-lzip: $(project-package-contents)
	curdir=$$(pwd)
	cd $(texdir)
	tar -cf $(project-package-name).tar $(project-package-name)
	if [ $@ = dist ]; then
	  suffix=gz
	  gzip -f --best $(project-package-name).tar
	elif [ $@ = dist-lzip ]; then
	  suffix=lz
	  lzip -f --best $(project-package-name).tar
	fi
	rm -rf $(project-package-name)
	cd $$curdir
	mv $(texdir)/$(project-package-name).tar.$$suffix ./

# Package into `.tar.gz' or '.tar.lz'.
dist-arxiv: dist-arXiv

dist-arXiv: $(project-package-contents)
	curdir=$$(pwd)
	cd $(texdir)/$(project-package-name)
	  tar -cv --exclude="./reproduce/*" --exclude="./project" -f ../$(project-package-name)-arXiv.tar .
	  suffix=gz
	  gzip -f --best ../$(project-package-name)-arXiv.tar
	  cd ..
	rm -rf $(project-package-name)
	cd $$curdir
	mv -v $(texdir)/$(project-package-name)-arXiv.tar.$$suffix ./

# Package into `.zip'.
dist-zip: $(project-package-contents)
	curdir=$$(pwd)
	cd $(texdir)
	zip -q -r $(project-package-name).zip $(project-package-name)
	rm -rf $(project-package-name)
	cd $$curdir
	mv $(texdir)/$(project-package-name).zip ./


# This line assumes that you have renamed your main git branch (formerly
# called the master branch) with the name of your package. Change this
# line appropriately if this is not the case.
#main-git-branch = $(package-name)
main-git-branch = N128cubed

git-bundle:
	git bundle create $(project-package-name-committed-only)-git.bundle HEAD $(main-git-branch)
	ls -l $(project-package-name-committed-only)-git.bundle

git-snapshot:
	git archive --format=tar.gz \
	   --prefix=$(project-package-name-committed-only)/ HEAD \
	   > $(project-package-name-committed-only)-snapshot.tar.gz
	ls -l $(project-package-name-committed-only)-snapshot.tar.gz

# Package the software tarballs.
dist-software:
	curdir=$$(pwd)
	dirname=software-$(project-commit-hash)
	cd $(bsdir)
	if [ -d $$dirname ]; then rm -rf $$dirname; fi
	mkdir $$dirname
	cp -L tarballs/* $$dirname/
	tar -cf $$dirname.tar $$dirname
	gzip -f --best $$dirname.tar
	rm -rf $$dirname
	cd $$curdir
	mv $(bsdir)/$$dirname.tar.gz ./





# Directory containing to-be-published datasets
# ---------------------------------------------
#
# Its good practice (so you don't forget in the last moment!) to have all
# the plot/figure/table data that you ultimately want to publish in a
# single directory.
#
# There are two types of to-publish data in the project.
#
#  1. Those data that also go into LaTeX (for example to give to LateX's
#     PGFPlots package to create the plot internally) should be under the
#     '$(texdir)' directory (because other LaTeX producers may also need it
#     for example when using './project make dist', or you may want to
#     publish the raw data behind the plots, like:
#     https://zenodo.org/record/4291207/files/tools-per-year.txt). The
#     contents of this directory are also directly taken into the tarball.
#
#  2. The data that aren't included directly in the LaTeX run of the paper,
#     can be seen as supplements. A good place to keep them is under your
#     build-directory.
#
# RECOMMENDATION: don't put the figure/plot/table number in the names of
# your to-be-published datasets! Given them a descriptive/short name that
# would be clear to anyone who has read the paper. Later, in the caption
# (or paper's tex/appendix), you will put links to the dataset on servers
# like Zenodo (see the "Publication checklist" in 'README-hacking.md').
tex-publish-dir = $(texdir)/to-publish
data-publish-dir = $(badir)/data-to-publish
$(tex-publish-dir):; mkdir $@
$(data-publish-dir):; mkdir $@




# Print Copyright statement
# -------------------------
#
# This statement can be used in published datasets that are in plain-text
# format. It assumes you have already put the data-specific statements in
# its first argument, it will supplement them with general project links.
print-copyright = \
	if [ "x$(strip $(2))" = "xnew" ]; then \
	   echo "\# Project title: $(metadata-title)" > $(1); \
	else \
	   echo "\# Project title: $(metadata-title)" >> $(1); \
	fi; \
	echo "\# Git commit (that produced this dataset): $(project-commit-hash)" >> $(1); \
	echo "\# Project's Git repository: $(metadata-git-repository)" >> $(1); \
	if [ x$(metadata-arxiv) != x ]; then \
	  echo "\# Pre-print server: https://arxiv.org/abs/$(metadata-arxiv)" >> $(1); fi; \
	if [ x$(metadata-doi-journal) != x ]; then \
	  echo "\# DOI (Journal): $(metadata-doi-journal)" >> $(1); fi; \
	if [ x$(metadata-doi-zenodo) != x ]; then \
	echo "\# DOI (Zenodo): $(metadata-doi-zenodo)" >> $(1); fi; \
	echo "\#" >> $(1); \
	echo "\# This data set is not copyrightable [1][2][3]." >> $(1); \
	echo "\# Attribution would be appreciated. :)" >> $(1); \
	echo "\# If necessary, you may consider the following to be the copyright terms:" >> $(1); \
	echo "\# Copyright (C) $$(date +%Y) $(metadata-copyright-owner)" >> $(1); \
	echo "\# This dataset is available under $(metadata-copyright)." >> $(1); \
	echo "\# License URL: $(metadata-copyright-url)" >> $(1); \
	printf "\#\n" >> $(1); \
	printf "\# [1] https://www.gnu.org/licenses/license-list.html\#ODbl\n" >> $(1); \
	printf "\# [2] https://law.stackexchange.com/questions/11359/can-you-copyright-data\n" >> $(1); \
	printf "\# [3] https://web.archive.org/web/20160304123939/http://www.lib.umich.edu/copyright/facts-and-data\n" >> $(1); \
	printf "\#\n" >> $(1)





# Project initialization results
# ------------------------------
#
# This file will store some basic info about the project that is necessary
# for the final PDF. Since these are not version controlled, it must be
# calculated everytime the project is run. So even though this file
# actually exists, it is also aded as a `.PHONY' target above.
include $(softconfdir)/TARGETS.conf

$(mtexdir)/initialize.tex: | $(mtexdir)

        # Version and title of project. About the starting '@': since these
        # commands are run every time with './project make', it is annoying
        # to print them on the standard output every time. With the '@',
        # make will not print the commands that it runs in this recipe.
	@d=$$(git show -s --format=%aD HEAD | awk '{print $$2, $$3, $$4}')
	echo "\newcommand{\projectdate}{$$d}" > $@
	echo "\newcommand{\projecttitle}{$(metadata-title)}" >> $@
	echo "\newcommand{\projectversion}{$(project-commit-hash)}" >> $@
	echo "\newcommand{\projectzenodoid}{$(metadata-zenodo-id)}" >> $@
	echo "\newcommand{\projectzenodohref}{\href{$(metadata-zenodo-url-base)}{$(metadata-zenodo-id)}}" >> $@
	if [ "x${enable_dev_override}" = "xTrue" ]; then \
	   echo "\newcommand{\projectzenodofilesbase}{$(metadata-zenodo-url-base-dev-override)/files}" >> $@; \
	else \
	   echo "\newcommand{\projectzenodofilesbase}{$(metadata-zenodo-url-base)/files}" >> $@;
	fi
	echo "\newcommand{\projectgitrepository}{\url{$(metadata-git-repository)}}" >> $@
	echo "\newcommand{\projectgitrepositoryarchived}{\href{$(metadata-git-repository-archived)}{swh:1:dir:$(metadata-swh-id)}}" >> $@
	echo "\newcommand{\projectgitrepositoryissues}{\url{$(metadata-git-repository)/issues}}" >> $@

	# Software target names:
	# Creates macros from foo-bar-3.14.159 such as:
	# \newcommand{\foobarname}{{\sc foo-bar}}
	# \newcommand{\foobarversion}{3.14.159}
	#
	# TODO: substitution of _ in versions by \_ has not yet been tested.
	for prog_raw in $(top-level-programs); do \
	  prog_filepath=$(installdir)/version-info/proglib/$${prog_raw} \
	  && prog_nameversion=$$(cat $$(ls -t $${prog_filepath}-*|head -n1)|tr -d '\n') \
	  && prog_name=$$(echo $${prog_nameversion}|awk '{print $$1}') \
	  && prog_name_tex=$$(echo $${prog_name}|tr -d '-'|tr -d '_') \
	  && prog_version=$$(echo $${prog_nameversion}|awk '{print $$2}'| sed -e 's/_/\\\\_/') \
	  && printf "\\\\newcommand{\\\\$${prog_name_tex}name}{{\\\\sc $${prog_name}}}\n" >> $@ \
	  && printf "\\\\newcommand{\\\\$${prog_name_tex}version}{$${prog_version}}\n" >> $@ ; \
	 done
	# These are for use in project-preamble.tex:
	echo "\newcommand{\projectgitrepo}{$(metadata-git-repository)}" >> $@
	echo "\newcommand{\projectcopyrightowner}{$(metadata-copyright-owner)}" >> $@

        # Calculate the latest Maneage commit used to build this
        # project:
        #  - The project may not have the 'maneage' branch (for example
        #    after cloning from a fork that didn't include it!). In this
        #    case, we'll print a descriptive warning, telling the user what
        #    should be done (reporting the last merged commit and its date
        #    is very useful for the future).
        #  - The '--dirty' option (used in 'project-commit-hash') isn't
        #    applicable to "commit-ishes" (direct quote from Git's error
        #    message!).
	if git log maneage -1 &> /dev/null; then
	  c=$$(git merge-base HEAD maneage)
	  v=$$(git describe --always --long $$c)
	  d=$$(git show -s --format=%aD $$v | awk '{print $$2, $$3, $$4}')
	else
	  echo
	  echo "WARNING: no 'maneage' branch found! Without it, the latest merge of "
	  echo "this project with Maneage can't be reported in the paper (which is bad "
	  echo "for your readers; that includes yourself in a few years). Please run "
	  echo "the commands below to fetch the 'maneage' branch from its own server "
	  echo "and remove this warning (these commands will not affect your project):"
	  echo "   $ git remote add origin-maneage http://git.maneage.org/project.git"
	  echo "   $ git fetch origin-maneage"
	  echo "   $ git branch maneage --track origin-maneage/maneage"
	  echo
	  v="\textcolor{red}{NO-MANEAGE-BRANCH (see printed warning to fix this)}"
	  d="\textcolor{red}{NO-MANEAGE-DATE}"
	fi
	echo "\newcommand{\maneagedate}{$$d}" >> $@
	echo "\newcommand{\maneageversion}{$$v}" >> $@


create_tex_macro = \
  filename=$(strip $(1)); \
  string_list=$(strip $(2)); \
  format=$(strip $(3)); \
  identifier=$(strip $(4)) \
    && param_list=$$( for param in $${string_list}; do egrep -e "$${param}" $${filename}|grep -v "^ *\#"|  \
        sed -e "s/\#.*\'//"  -e 's/\([a-zA-Z]\) *\([0-9]\)/\1=\2/'|sed -e "s/\#.*\'//"| tr -d ' '|tr '\n' ' ' ; done) \
    && printf "param_list=$${param_list}\n\n" \
    && for param_line in $${param_list}; do \
      param_name_orig=$$(echo $${param_line}|awk -F '=' '{print $$1}') \
      && param_name_tex="$${identifier}"$$(printf "$${param_name_orig}"|sed -e 's/[^_-]*/\u&/g' |tr -d '_'|tr -d '-') \
      && param_name=$$(echo $${param_name_orig}|sed -e 's/_/\\\\_/g') \
      && if [ x$${format} = "xstr" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf $$2}' ); \
         fi \
      && if [ x$${format} = "xint" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.2f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat.0" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.0f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat.1" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.1f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat.3" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.3f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat.4" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.4f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xexp" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.2e" , $$2}' | sed -e 's/[eE]\(\(\|-\)\|+\)\([0-9][0-9]*\)/\\\\times 10^{\2\3}/' | \
	                  sed -e 's/1\.0*..times//'); \
         fi \
      && if [ x$${format} = "xexp.1" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.1e" , $$2}' | sed -e 's/[eE]\(\(\|-\)\|+\)\([0-9][0-9]*\)/\\\\times 10^{\2\3}/' | \
	                  sed -e 's/1\.0*..times//'); \
         fi \
      && if [ x$${format} = "xstr" ]; then\
          param_value=$$(echo $${param_line}| \
	  sed -e 's/;/ /g' | \
          awk -F '=' '{printf "%s", $$2}' ); \
         fi \
      && printf "\\\\newcommand{\\\\$${param_name_tex}name}{{\\\\tt $${param_name}}}\n" >> $@ \
      && printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@ \
    ; done
