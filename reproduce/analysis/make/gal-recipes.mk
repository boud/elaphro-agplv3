# Call semi-analytical tools to model galaxies.
#
# (C) 2020 Boud Roukema, Marius Peper, GPL-3+
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

## Include the initial conditions parameters:
#sim_paramsfile = $(pconfdir)/init-conditions-params.conf
gal_recipes_paramsfile = $(pconfdir)/create-galaxies-params.conf

#include ${sim_paramsfile}
#include ${gal_recipes_paramsfile}


#ibdir = $(bsdir)/installed/bin
#tree_dir = $(badir)/mergertrees
#gal_dir = $(badir)/galaxies
#haloes_dir = $(badir)/haloes
patchesdir = /reproduce/software/make
# 'model_z*' because there is no unique filename - should this work?
#galaxies_output_file = $(gal_dir)/sage_out/model_z*
sage_namelist_output_file = $(gal_dir)/gal.par
done_create_galaxies_file = $(gal_dir)/done-create-galaxies

# aliases
.PHONY : sage
sage: create-galaxies

# This is the main rule.
.PHONY : create-galaxies
create-galaxies: $(done_create_mergertree_file) $(converter_output_file) $(done_create_galaxies_file) $(mtexdir)/gal-recipes.tex

$(done_create_galaxies_file): $(done_create_mergertree_file) $(sim_paramsfile) $(gal_recipes_paramsfile)
	rm -frv $(gal_dir)/sage_out/model_z*
	rm -fr $(badir)/galaxies
	cp -pv $(bashdir)/run-sage $(ibdir)
	mkdir -p $(gal_dir)
	cd $(gal_dir)
	mkdir -p sage_out
	mkdir -p ./output/pegase_inputs
	mkdir -p ./output/add_outs
	cp -pvdr $(bsdir)/installed/extra .
	IN_DIR=$(tree_dir)/converter_out HALO_DIR=$(haloes_dir) \
	 TREE_DIR=$(tree_dir) HUBBLE0=$(Hubble) \
	 OMEGA_M=$(OmegaM) OMEGA_L=$(OmegaL) \
	 BARYON_FRAC=$(baryon_frac) \
	 INSTALLED_BIN_DIR=$(ibdir) \
	 run-sage
	succesful_run=$$(grep -E 'Output [0-9]* had [0-9]* galaxies|end of sage' sage.log)
	if [ "x$${succesful_run}" == "x" ] ; then
	   printf "Error when running sage\n"
	   exit 1
	else
	   touch $(done_create_galaxies_file)
	   printf "Sage ran successfully. See the new files in $(gal_dir)/ .\n"
	fi

$(mtexdir)/gal-recipes.tex: $(done_create_galaxies_file)
        # This section produces a file with LaTeX macros for some of
        # the parameters in $(sage_namelist_output_file). It presently
        # selects a small hardwired list of parameters rather than all
        # of them.  This section will be re-run if $(sage_namelist_output_file) is
        # updated.
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	param_list="Omega BaryonFrac Hubble_h PartMass"
	$(call create_tex_macro, $(sage_namelist_output_file), $${param_list}, float, sageparam)
	printf "\\\\newcommand{\\\\HowWeFormedStars}{This is how we created the galaxies from their star formation histories.}\n\n" >> $@
	printf "\nThe LaTeX macro file $@ was created.\n\n"

clean-galaxies:
	rm -fr $(badir)/galaxies
	rm -f $(done_create_galaxies_file)

