# Detect haloes in a simulation.
#
# (C) 2020 Boud Roukema, Marius Peper, GPL-3+
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

## Include the initial conditions parameters:
#include reproduce/analysis/config/init-conditions-params.conf

#ibdir = $(bsdir)/installed/bin
haloes_dir = $(badir)/haloes
sim_out_dir = $(badir)/n-body-init/simulation-out
halo_paramsfile = $(pconfdir)/detect-haloes-params.conf
#init_dir = $(badir)/n-body-init
example_haloes_output_file = $(haloes_dir)/halos_1.0.ascii
rockstar_namelist_output_file = $(haloes_dir)/rockstar.cfg
done_detect_haloes_file = $(haloes_dir)/done-detect-haloes

#include ${halo_paramsfile}

# aliases
.PHONY : rockstar
rockstar: detect-haloes

# This is the main rule.
# At least one typical haloes output file is needed; and we need to
# regenerate TeX macros if necessary.
.PHONY : detect-haloes
detect-haloes: $(example_haloes_output_file) $(mtexdir)/detect-haloes.tex

## We do not want to re-run ramses automatically. The target file
## that has the rule for running ramses is $(example_sims_output_file).
## Instead of an automatic link, we set a dependence on another file
## that `make` does not know how to create within its rules. Instead,
## `make` has a rule with a sleep cycle to see if the file has been
## created. This file is $(done_run_simulation_file).

$(done_detect_haloes_file): $(example_haloes_output_file)

# The simulations output files must already exist; the haloes output
# files need to be made to satisfy this rule.
rockstar_pid_file = rockstar_pid_maybe_active

$(example_haloes_output_file): $(done_run_simulation_file) $(halo_paramsfile)
	cp -pv $(bashdir)/run-rockstar $(ibdir)
	if [ -e  $(haloes_dir)/$(rockstar_pid_file) ]; then
	   printf "Killing possible rockstar orphan processes...\n"
	   ((for pid in $$(cat $(haloes_dir)/$(rockstar_pid_file)); do
	     kill -9 $${pid}
	     done) || true)
	fi
	rm -frv $(badir)/haloes
	mkdir -p $(haloes_dir)
	cd $(haloes_dir)
	rm -fv haloes-failed
	cp -pv $(ibdir)/rockstar $(haloes_dir)/rockstar
	LEVELMAX=$(ramses_levelmax) LBOX=$(Lbox)
	 HALO_DIR=$(haloes_dir) SIM_OUT_DIR=$(sim_out_dir) \
	 STARTING_SNAP=$(rockstar_starting_snap) FOF_LINKING_LENGTH=$(rockstar_fof_linking_length) \
	 FOF_FRACTION=$(rockstar_fof_fraction) \
	 MIN_HALO_OUTPUT_SIZE=$(rockstar_min_halo_output_size) \
	 MIN_HALO_PARTICLES=$(rockstar_min_halo_particles) \
	 MASS_DEFINITION=$(rockstar_mass_definition) \
	 run-rockstar
	if ! ([ -e haloes-failed ]) ; then
	   touch $(done_detect_haloes_file)
	   printf "\nRockstar was successfully run. See the new files in $(haloes_dir)/\n\n"
	else
	   rm $(done_detect_haloes_file)
	   printf "Failed run of rockstar.\n"
	   exit 1
	fi

$(mtexdir)/detect-haloes.tex: $(halo_paramsfile) $(done_detect_haloes_file)
        # This section produces a file with LaTeX macros for some of
        # the parameters in $(rockstar_namelist_output_file). It presently
        # selects a small hardwired list of parameters rather than all
        # of them.  This section will be re-run if $(rockstar_namelist_output_file) is
        # updated.
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	grep -v "MASS_DEFINITION[2-9]" $(rockstar_namelist_output_file) > $(rockstar_namelist_output_file).tmp
	param_list_float="FOF_LINKING_LENGTH BOX_SIZE"
	param_list_int="MIN_HALO_OUTPUT_SIZE"
	param_list_str="MASS_DEFINITION"
	$(call create_tex_macro, $(rockstar_namelist_output_file).tmp, $${param_list_float}, float, rockstarparam)
	$(call create_tex_macro, $(rockstar_namelist_output_file).tmp, $${param_list_int}, int, rockstarparam)
	$(call create_tex_macro, $(rockstar_namelist_output_file).tmp, $${param_list_str}, str, rockstarparam)
	printf "\\\\newcommand{\\\\HowWeDetectedHaloes}{This is how we detected haloes.}\n\n" >> $@
	printf "\nThe LaTeX macro file $(mtexdir)/detect-haloes.tex was created.\n\n"

clean-haloes:
	rm -fv $(example_haloes_output_file) $(done_detect_haloes_file)
	if [ -e  $(haloes_dir)/$(rockstar_pid_file) ]; then
	  printf "Killing possible rockstar orphan processes...\n"
	  for pid in $$(cat $(haloes_dir)/$(rockstar_pid_file)); do
	     (kill -9 $${pid} || true)
	  done
	fi
	rm -frv $(badir)/haloes
