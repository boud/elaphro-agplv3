GNU Affero General Public Licensing of Elaphrocentre
====================================================

Copyright (C) 2018-2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>
Copyright (C) 2020-2021 Marius Peper, Boud Roukema
See the end of the file for license conditions.

**Context**: Scientific communication requires a software user to
have the freedom to check the validity of software, to improve
the software, and to disseminate it with his or her peers. The
older GPL versions have a legal loophole: software run on a
remote server is not directly run by the end user, so even though
(apparently) FaceBook runs on Fedora GNU/Linux and Google runs on Debian
GNU/Linux, using powerful free-licensed software, their end users are disempowered.
In contrast, the Wikidot and Mastodon software packages are GNU AGPLv3 licensed,
empowering their users. A world-famous source of pseudo-science and "false facts"
is currently testing the legal power of the GNU AGPLv3 ([Clark 29 Oct 2021][TheVerge]).

**Aim**: could the elaphrocentre project be fully relicensed under
the [GNU AGPLv3][GNUAGPLv3] (Wikipedia: [WP-GNUAGPL])?

**Method**: Analyse the component software packages used by the elaphrocentre
project; read what licence each package claims it uses; do some checking
for missing/misleading licences; add entries to the file
'reproduce/software/conf/agplv3orlater.conf' for each package.

Possible parameters per package could be, for package called PACKAGE:

* PACKAGE_current_licence_main = [max 1 string]
* PACKAGE_current_licence_other = [strings separated by commas]
* PACKAGE_is_agplv3orlater = [yes|no]
* PACKAGE_is_agplv3only = [yes|no]
* PACKAGE_is_agplv3orlater_compatible = [yes|no]
* PACKAGE_emails_of_authors_to_negotiate_with = [email strings, comma-separated; empty if agplv3orlater == yes]
* PACKAGE_probability_of_authors_accepting_agplv3orlater = [float in range 0.0 to 1.0; automatic value of 1 if agplv3orlater == yes] # bayesian estimate
* PACKAGE_copyright_upgrade_difficulty = [float in range 0.0 to 1.0] # bayesian estimate


[GNUAGPLv3]: https://www.gnu.org/licenses/agpl-3.0.en.html "GNU AGPLv3"
[WP-GNUAGPL]: https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License "Wikipedia GNU AGPLv3"
[TheVerge]: https://www.theverge.com/2021/10/29/22752850/mastodon-trump-truth-social-network-open-source-gab-legal-notice "Mitchell Clark, The Verge, 29 Oct 2021, 'Mastodon puts Trump's social network on notice for improperly using its code'"



### Copyright information

This file and `.file-metadata` (a binary file, used by Metastore to store
file dates when doing Git checkouts) are part of the reproducible project
mentioned above and share the same copyright notice (at the start of this
file) and license notice (below).

This project is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This project is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this project.  If not, see <https://www.gnu.org/licenses/>.
